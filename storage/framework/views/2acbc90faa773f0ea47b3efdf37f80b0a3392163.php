
<?php $__env->startSection('title'); ?> Addons Manager <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">Addons Install Manager</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-2 installui">

            

<div class="container m-auto">
  <div class="row my-4">
      
    <div class="col-3">
        <a href="<?php echo e(route('addon.setup', 'paytm')); ?>">
            <div class="card setup-card">
                <img class="card-img-top setup-card-img" src="<?php echo e(filePath('paytm-thumb.png')); ?>" alt="#paytm">
            </div>
        </a>
    </div>


    <div class="col-3">
        <a href="<?php echo e(route('addon.setup', 'product_export_import')); ?>">
            <div class="card setup-card">
                <img class="card-img-top setup-card-img" src="<?php echo e(filePath('product-ex-im-thumb.png')); ?>" alt="#product_export_import">
            </div>
        </a>
    </div>


    <div class="col-3">
        <a href="<?php echo e(route('addon.setup', 'ssl_commerz')); ?>">
            <div class="card setup-card">
                <img class="card-img-top setup-card-img" src="<?php echo e(filePath('ssl-commerz-thumb.png')); ?>" alt="#ssl_commerz">
            </div>
        </a>
    </div>


     <div class="col-3">
      <a href="<?php echo e(route('addon.setup', 'affiliate_marketing')); ?>">
          <div class="card setup-card">
              <img class="card-img-top setup-card-img" src="<?php echo e(filePath('affiliate-thumb.png')); ?>" alt="#affiliate_marketing">
          </div>
      </a>
  </div>


  
</div>
</div>


            

        </div>

    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/addons/install.blade.php ENDPATH**/ ?>