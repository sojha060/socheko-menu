
<?php $__env->startSection('title'); ?> Addons Manager <?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title m-2"> Addons Manager</h3>

            <a class="btn btn-primary ml-3" href="<?php echo e(route("addons.manager.installui")); ?>" title="Install new addon">
                <i class="fa fa-plus-circle"></i> Install Addon
            </a>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-2">

            

            

<div class="wrapper">
    <?php if(paytmActive()): ?>
    <h4><strong>Installed Addon(s) ( <?php echo e(count($addons)); ?> )</strong></h4>
    <?php else: ?>
    <h4><strong>Installed Addon(s) ( 0 )</strong></h4>
    <?php endif; ?>
    
    <div class="row mt-5">
 
            <?php $__empty_1 = true; $__currentLoopData = $addons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $addon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-4 mt-3">
                    <div class="news">
                        <figure class="article">
                            <a href="javscript:void()" onclick="forModal('<?php echo e(route('addon.preview', $addon->name)); ?>', '<?php echo e(strtoupper($addon->name)); ?>')">
                                <img src="<?php echo e(filePath($addon->image)); ?>" class="w-100 img-fluid rounded" alt="#<?php echo e($addon->name); ?>" >
                            </a>
                        </figure>
                            <a href="<?php echo e(route('addon.status', $addon->name)); ?>" class="btn btn-<?php echo e($addon->activated == 0 ? 'success' : 'danger'); ?> w-100"><?php echo e($addon->activated == 0 ? 'Activate' : 'Deactivate'); ?></a>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-12">
                        <img src="<?php echo e(filePath('no-addon-found.jpg')); ?>" class="w-100" alt="#No Addons Found">
                </div>
            <?php endif; ?>

    </div>
</div>

            
            <hr>
            
            <div class="wrapper">
                <h4><strong>Available Addon(s) ( <span id="addon_count"></span> )</strong></h4>

                <input type="hidden" id="sk_loading" value="<?php echo e(filePath('sk-loading.gif')); ?>">

                <div id="no-addons" class="text-center"></div>
                <div id="no-addons-found" class="text-center d-none">
                    <img src="<?php echo e(filePath('no-addon-found.jpg')); ?>" class="w-100" alt="#No Addons Found">
                </div>
                
                <div class="d-flex mt-5" id="available_addons"></div>
            </div>
            

            

        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script>

$(document).ready(function(){

    var username = 'softtech-it';
    var site = 'codecanyon';
    var code = '0eZScyN9HOoPHzKSJMtWI8U1d7VwkApX';
    var sk_loading = $('#sk_loading').val(); 

    $('#no-addons').html('<img src="'+ sk_loading +'" class="w-75 mt-5" alt="#Loading">');

    $.ajax({
        type: "GET",
        headers: {
            "Authorization": "Bearer " + code
        },
        url:'https://api.envato.com/v1/market/new-files-from-user:'+ username +','+ site +'.json',

        success: function( response) {
            var data = response['new-files-from-user'];
            
            var addons_count = 0;


            $('#no-addons').addClass('d-none');
            $('#no-addons-found').addClass('d-none');

            var addons = '';
        
            data.forEach(element => {

                if (element.tags.match('manyvendor add-on')) {

                 addons += '<div class="col-4 mt-3">' +
                                    '<div class="news">'+
                                        '<figure class="article">'+
                                            '<img src='+ element.live_preview_url +' class="w-100 rounded" alt="#'+ element.item +'">'+
                                    ' </figure>'+
                                        '<a href='+ element.url +' class="btn btn-primary w-100" "target=_blank">Buy Now</a>'+
                                    '</div>'+
                            '</div>'

                    addons_count += 1;
                }
            });
            
            if (addons_count > 0) {
                var addons_count = $('#addon_count').html(addons_count);
            } else {
                var addons_count = $('#addon_count').html(0);
                $('#no-addons-found').removeClass('d-none');
            }

            $('#available_addons').html(addons); 
        }
    });
});

</script>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/addons/index.blade.php ENDPATH**/ ?>