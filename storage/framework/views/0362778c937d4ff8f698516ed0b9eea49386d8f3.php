<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?php echo e(getSystemSetting('type_name')); ?> | <?php echo $__env->yieldContent('title'); ?></title>


    <?php echo $__env->make('backend.layouts.includes.style', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">


</head>

<body class="hold-transition sidebar-mini layout-fixed">

<div class="limiter">
    <div class="container-login100">
        <div class="login100-more"></div>
        <div class="wrap-login100 p-l-50 p-r-50 p-t-100 p-b-50">
            <div class="mt-auto">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </div>
</div>



<?php echo $__env->make('backend.layouts.includes.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/auth/app.blade.php ENDPATH**/ ?>