

<?php $__env->startSection('keywords'); ?>
<?php echo e($single_product->meta_desc); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
- <?php echo e($single_product->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <nav class="navigation--mobile-product">
        <?php if(auth()->guard()->check()): ?>
            <a class="ps-btn ps-btn--black" href="#!" onclick="addToWishlist(<?php echo e($single_product->id); ?>)"><i
                        class="icon-heart"></i></a>
        <?php endif; ?>
        <?php if(auth()->guard()->guest()): ?>
          

                        <a class="ps-btn ps-btn--black" href="javascript:void()" onclick="addProduct(<?php echo e($single_product->id); ?>)"><i
                        class="icon-heart"></i></a>

        <?php endif; ?>
        <a class="ps-btn" href="javascript:void()" onclick="addToCompare(<?php echo e($single_product->id); ?>)"><i class="fa fa-random"></i></a>
    </nav>
    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                
                <li>
                    <a href="<?php echo e(route('category.shop', $single_product->childcategory->name)); ?>"><?php echo e($single_product->childcategory->name); ?></a>
                </li>
                <li><?php echo e($single_product->name); ?></li>
            </ul>
        </div>
    </div>
    <input id="productId" type="hidden" value="<?php echo e($single_product->id); ?>">
    <div class="ps-page--product">
        <div class="ps-container">
            <div class="ps-page__container">
                <div class="ps-page__left">
                    <div class="ps-product--detail ps-product--fullwidth">
                        <div class="ps-product__header">
                            <div class="ps-product__thumbnail" data-vertical="true">
                                <figure>
                                    <div class="ps-wrapper">
                                        <div class="ps-product__gallery" data-arrow="true">

                                            <?php if(count($single_product->images) > 0): ?>

                                            <?php $__currentLoopData = $single_product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="item">
                                                    <a href="<?php echo e(filePath($image->image)); ?>">
                                                        <img src="<?php echo e(filePath($image->image)); ?>" class="m-auto" alt="">
                                                    </a>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php else: ?>

                                                <div class="item">
                                                    <a href="<?php echo e(filePath($single_product->image)); ?>">
                                                        <img src="<?php echo e(filePath($single_product->image)); ?>" class="m-auto" alt="<?php echo e($single_product->name); ?>">
                                                    </a>
                                                </div>
                                                
                                            <?php endif; ?>
                                            

                                        </div>
                                    </div>
                                </figure>
                                <div class="ps-product__variants" data-item="4" data-md="4" data-sm="4"
                                     data-arrow="false">
                                    <?php $__currentLoopData = $single_product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="item"><img src="<?php echo e(filePath($image->image)); ?>" alt=""></div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                            <div class="ps-product__info">
                                <h1><?php echo e($single_product->name); ?></h1>
                                
                                
                                <div class="ps-product__desc">
                                    <p>
                                        <?php echo Purify::clean($single_product->short_desc); ?>

                                    </p>
                                </div>
                                <div class="ps-product__variations">
                                    <figure>
                                        <label for="d-block">
                                                Select Variation
                                            </label>
                                        <div class="form-row">
                                            

                                            

                                            <?php $__currentLoopData = $product_variants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($variant->unit == 'Color'): ?>
                                                    <input type="radio" name="<?php echo e($variant->unit); ?>"
                                                           id="color<?php echo e($variant->code); ?>" value="<?php echo e($variant->id); ?>"/>
                                                    <label for="color<?php echo e($variant->code); ?>" class="variant_label">
                                                        <span class="variant_color card"
                                                              style="background: <?php echo e($variant->code ?? ''); ?>;"></span>
                                                    </label>
                                                <?php else: ?>
                                                    <input type="radio" id="unit-<?php echo e($variant->variant); ?>"
                                                           name="<?php echo e($variant->unit); ?>" value="<?php echo e($variant->id); ?>"/>
                                                    <label for="unit-<?php echo e($variant->variant); ?>"
                                                           class="variant_unit"><?php echo e($variant->variant); ?></label>

                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </figure>
                                </div>
                                <div class="ps-product__shopping">
                                    <figure class="mb-sm-2">
                                        <figcaption>Quantity</figcaption>


                                        <div class="value-button" id="decrease" onclick="decreaseValue()"
                                             value="Decrease Value">-
                                        </div>

                                        <input type="number"
                                               id="number"
                                               value="1"
                                               min="1"
                                               max="10"
                                               class="cart-quantity input-number"
                                               readonly
                                        />

                                        <div class="value-button" id="increase" onclick="increaseValue()"
                                             value="Increase Value">+
                                        </div>

                                    </figure>
                                    <a class="ps-btn bookWithoutVariant mt-3" id="check_shop" href="#shops"
                                       data-toggle="tooltip" data-placement="top"
                                       data-title="Please select the product variant">Check available shop</a>
                                    <div id="shops"></div>
                                    <div class="ps-product__actions active mt-2">

                                        <?php if(auth()->guard()->check()): ?>
                                            <a href="#!" onclick="addToWishlist(<?php echo e($single_product->id); ?>)" 
data-toggle="tooltip" data-placement="top" data-title="Add to wishlist"><i
                                                        class="icon-heart"></i></a>
                                        <?php endif; ?>
                                        <?php if(auth()->guard()->guest()): ?>
                                            <a href="#!" 
                                        class="wishlist" 
                                        data-placement="top" 
                                        data-title="Add to wishlist"
                                        data-toggle="tooltip" 
                                        data-product_name='<?php echo e($single_product->name); ?>' 
                                        data-product_id='<?php echo e($single_product->id); ?>' 
                                        data-product_sku='<?php echo e($single_product->sku); ?>' 
                                        data-product_slug='<?php echo e($single_product->slug); ?>' 
                                        data-product_image='<?php echo e(filePath($single_product->image)); ?>' 
                                        data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                        data-product_price='<?php echo e(formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                           == formatPrice(brandProductPrice($single_product->sellers)->max())
                                                                           ? formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                           : formatPrice(brandProductPrice($single_product->sellers)->min()).
                                                                           '-' .formatPrice(brandProductPrice($single_product->sellers)->max())); ?>'
                                                    >
                                                    <i class="icon-heart"></i>
                                        </a>
                                        <?php endif; ?>


                                        <a href="#!" onclick="addToCompare(<?php echo e($single_product->id); ?>)" 
data-toggle="tooltip" data-placement="top" data-title="Add to comparison"><i
                                                    class="fa fa-random"></i></a>
                                    </div>
                                </div>
                                <div class="ps-product__specification">
                                    <p><strong>SKU:</strong> <?php echo e($single_product->sku); ?></p>
                                    <p class="categories"><strong> Categories:</strong>
                                        
                                        <a href="<?php echo e(route('category.shop',$single_product->childcategory->slug)); ?>"> <?php echo e($single_product->childcategory->name); ?></a>
                                    </p>
                                    <p class="tags"><strong> Tags</strong>
                                        <?php $__currentLoopData = json_decode($single_product->tags); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="javascript:void()"><?php echo e($data); ?></a>,
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </p>
                                </div>


                                
                                <div class="fb-share-button" data-href="<?php echo e(url()->full()); ?>" data-layout="button"
                                     data-size="small">
                                    <a target="_blank"
                                       href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F127.0.0.1%3A8000%2F&amp;src=sdkpreparse"
                                       class="fb-xfbml-parse-ignore">
                                        Share
                                    </a>
                                </div>
                            </div>
                        </div>


                        <!-- Vendor Shop -->


                        <div class="section__header">
                            <h3>Available Shop(s)</h3>
                        </div>
                        <div class="row p-lr-20 seller-div">
                            <?php $__empty_1 = true; $__currentLoopData = $single_product->sellers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $seller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <?php
                                    $vps = \App\Models\VendorProductVariantStock::where('user_id',$seller->user_id)->
                                    where('vendor_product_id',$seller->id)->where('product_id',$seller->product_id)->first();
                                ?>
                                <?php if($seller != null && $vps != null): ?>
                                    <div class="col-md-3">
                                        <aside class="widget widget_same-brand widget_vendor-shop">
                                            <div class="widget__content widget_vendor-shop-content">
                                                <div class="ps-product">
                                                    <div class="ps-product__thumbnail">
                                                        <?php if(empty($seller->user->vendor->shop_logo)): ?>
                                                            <a href="<?php echo e(route('vendor.shop',$seller->user->vendor->slug)); ?>">
                                                                <img height="100px" src="<?php echo e(asset('vendor-store.jpg')); ?>"
                                                                     class="rounded"
                                                                     alt="#<?php echo e($seller->user->vendor->shop_name); ?>">
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="<?php echo e(route('vendor.shop',$seller->user->vendor->slug)); ?>">
                                                                <img height="100px"
                                                                        src="<?php echo e(filePath($seller->user->vendor->shop_logo)); ?>"
                                                                        class="rounded"
                                                                        alt="#$seller->user->vendor->shop_name">
                                                            </a>
                                                        <?php endif; ?>

                                                    </div>
                                                    <div class="ps-product__container text-center">
                                                        <div class="ps-product__content">
                                                            <a class="ps-product__title"
                                                               href="<?php echo e(route('vendor.shop',$seller->user->vendor->slug)); ?>"><?php echo e($seller->user->vendor->shop_name); ?></a>

                                                            <?php

                                                                $stars_count = App\Models\OrderProduct::where('shop_id', $seller->user->vendor->id)
                                                                            ->whereNotNull('review_star')
                                                                            ->select('review_star')
                                                                            ->get()
                                                                            ->toArray();

                                                                $shop_stars_count = App\Models\OrderProduct::where('shop_id', $seller->user->vendor->id)
                                                                            ->whereNotNull('review_star')
                                                                            ->select('review_star')
                                                                            ->count();

                                                                $rateArray =[];
                                                                foreach ($stars_count as $star_count)
                                                                {
                                                                    $rateArray[]= $star_count['review_star'];
                                                                }
                                                                $sum = array_sum($rateArray);
                                                                

                                                                $customer_count = App\Models\OrderProduct::where('shop_id', 1)
                                                                                    ->whereNotNull('review_star')
                                                                                    ->count();
                                                                if ($customer_count > 0) {
                                                                    $result= round($sum/$customer_count);
                                                                }else {
                                                                    $result= round($sum/1);
                                                                }

                                                            ?>


                                                            <div class="br-wrapper br-theme-fontawesome-stars">
                                                                <div class="br-widget br-readonly">
                                                                    <?php if($result > 0): ?>
                                                                        <?php for($i = 0; $i < $result; $i++): ?>
                                                                            <a href="javascript:void()"
                                                                               data-rating-value="1"
                                                                               data-rating-text="1"
                                                                               class="br-selected br-current"></a>
                                                                        <?php endfor; ?>
                                                                    <?php else: ?>
                                                                        <span>No Rating</span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                            <p class="ps-product__price sale">
                                                                <?php if($seller->is_discount === 1): ?>
                                                                    <span><?php echo e(formatPrice($seller->discount_price)); ?></span>
                                                                    <del><?php echo e(formatPrice($seller->product_price)); ?></del>
                                                                <?php else: ?>
                                                                    <?php echo e(formatPrice($seller->product_price)); ?>

                                                                <?php endif; ?>
                                                            </p>
                                                        </div>
                                                        <?php if(auth()->guard()->check()): ?>
                                                            
                                                            <?php if($vps->quantity <= 1): ?>
                                                                <a href="#!"
                                                                   class="btn btn-danger m-2 p-3 fs-12  bookWithoutVariant"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   data-title="Please select the product variant">Out of stock</a>
                                                            <?php else: ?>
                                                                <a href="#!"
                                                                   class="btn btn-primary m-2 p-3 fs-12 addToCart-<?php echo e($vps->id); ?> bookWithoutVariant"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   data-title="Please select the product variant"
                                                                   onclick="addToCart(<?php echo e($vps->id); ?>)">Buy Now</a>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <?php if(auth()->guard()->guest()): ?>
                                                            
                                                            <?php if($vps->quantity <= 1): ?>
                                                                <a href="#!"
                                                                   class="btn btn-danger m-2 p-3 fs-12  bookWithoutVariant"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   data-title="Please select the product variant">Out of stock</a>
                                                            <?php else: ?>
                                                                <a href="#!"
                                                                   class="btn btn-primary m-2 p-3 fs-12 addToGuestCart-<?php echo e($vps->id); ?> bookWithoutVariant"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   data-title="Please select the product variant"
                                                                   onclick="addToGuestCart(<?php echo e($vps->id); ?>)">Buy Now</a>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                <?php endif; ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                <img src="<?php echo e(asset('shop-not-found.png')); ?>" alt="">

                            <?php endif; ?>

                        </div>

                        <!-- Vendor Shop END -->

                        <div class="ps-product__content ps-tab-root">
                            <ul class="ps-tab-list">
                                <li class="active"><a href="#tab-1">Description</a></li>
                                <li>
                                    <a href="#tab-4">Reviews (<?php echo e($reviews_count = 0 ? 0 : $reviews_count); ?>)
                                    </a>
                                </li>
                            </ul>
                            <div class="ps-tabs">
                                <div class="ps-tab active" id="tab-1">
                                    <div class="ps-document">
                                        <?php echo Purify::clean($single_product->big_desc); ?>

                                    </div>
                                </div>

                                <div class="ps-tab" id="tab-4">
                                    <div class="row">

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <hr/>
                                                    <div class="review-block">
                                                        <?php $__empty_1 = true; $__currentLoopData = $order_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                            <?php if(!empty($order_product->review)): ?>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <img
                                                                                src="<?php echo e(filePath($order_product->user->avatar )); ?>"
                                                                                class="img-rounded">
                                                                        <div
                                                                                class="review-block-name"><?php echo e($order_product->user->name); ?></div>
                                                                        <div
                                                                                class="review-block-date"><?php echo e($order_product->updated_at->format('M d, Y')); ?>

                                                                            <br/><?php echo e($order_product->updated_at->diffForHumans()); ?>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="review-block-rate">

                                                                            <?php for($i = 0; $i < $order_product->review_star; $i++): ?>
                                                                                <button type="button"
                                                                                        class="btn btn-warning btn-xs"
                                                                                        aria-label="Left Align">
                                                                                    <span class="fa fa-star"
                                                                                          aria-hidden="true"></span>
                                                                                </button>
                                                                            <?php endfor; ?>

                                                                        </div>

                                                                        <div
                                                                                class="review-block-description"><?php echo e($order_product->review); ?></div>
                                                                    </div>
                                                                </div>
                                                                <hr/>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                                            <img src="<?php echo e(asset('no-review.jpg')); ?>" alt="#no-review">

                                                        <?php endif; ?>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ps-page__right">
                    <aside class="widget widget_product widget_features">
                        <?php $__currentLoopData = infopage('top',4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <p>
                                <?php if($p->icon != null): ?>
                                    <i class="<?php echo e($p->icon); ?>"></i>
                                <?php endif; ?>
                                <?php if($p->page != null): ?>
                                    <a href="<?php echo e(route('frontend.page',$p->page->slug)); ?>">
                                        <?php endif; ?>
                                        <?php if($p->header != null): ?>
                                            <?php echo e($p->header); ?>

                                        <?php endif; ?>
                                        <?php if($p->page != null): ?>
                                    </a>
                                <?php endif; ?>
                            </p>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </aside>
                    <aside class="widget widget_sell-on-site">
                        <p><i class="icon-store"></i> Sell on <?php echo e(getSystemSetting('type_name')); ?>?<a
                                    href="<?php echo e(route('vendor.signup')); ?>"> Register Now !</a></p>
                    </aside>
                    <aside class="widget widget_ads"><a href="#"><img src="img/ads/product-ads.png" alt=""></a></aside>


                    <aside class="widget widget_same-brand">
                        
                    </aside>
                </div>
            </div>

            <div class="ps-section--default">
                <div class="ps-section__header">
                    <h3>Related products</h3>
                </div>
                <div class="ps-section__content">

                    <?php if($products_count < 0): ?>

                        <div class="row">
                            <div class="col-md-2 t-mb-30">
                                <a href="<?php echo e(route('single.product',[$single_product->sku,$single_product->slug])); ?>"
                                   class="product-card text-center">
                                <span class="product-card__action d-flex flex-column align-items-center ">
                                    <span class="product-card__action-is product-card__action-view"
                                          onclick="forModal('<?php echo e(route('quick.view',$single_product->slug)); ?>', 'Product quick view')">
                                    <i class="fa fa-eye"></i>
                                    </span>
                                    <span class="product-card__action-is product-card__action-compare"
                                          onclick="addToCompare(<?php echo e($single_product->id); ?>)">
                                    <i class="fa fa-random"></i>
                                    </span>
                                    <?php if(auth()->guard()->check()): ?>
                                        <span class="product-card__action-is product-card__action-wishlist"
                                              onclick="addToWishlist(<?php echo e($single_product->id); ?>)">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                    <?php endif; ?>

                                    <?php if(auth()->guard()->guest()): ?>
                                        <span 
                                                                class="product-card__action-is product-card__action-wishlist wishlist"
                                                                data-placement="top" 
                                                                data-title="Add to wishlist"
                                                                data-toggle="tooltip" 
                                                                data-product_name='<?php echo e($single_product->name); ?>' 
                                                                data-product_id='<?php echo e($single_product->id); ?>' 
                                                                data-product_sku='<?php echo e($single_product->sku); ?>' 
                                                                data-product_slug='<?php echo e($single_product->slug); ?>' 
                                                                data-product_image='<?php echo e(filePath($single_product->image)); ?>' 
                                                                data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                                                data-product_price='<?php echo e(formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                                                == formatPrice(brandProductPrice($single_product->sellers)->max())
                                                                                                ? formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                                                : formatPrice(brandProductPrice($single_product->sellers)->min()).
                                                                                                '-' .formatPrice(brandProductPrice($single_product->sellers)->max())); ?>'>
                                                                    <i class="fa fa-heart-o"></i>
                                                                </span>
                                    <?php endif; ?>
                                </span>
                                    <span class="product-card__img-wrapper m-auto text-center w-50">
                                    <img src="<?php echo e(filePath($single_product->image)); ?>" alt="#<?php echo e($single_product->name); ?>"
                                         class="img-fluid mx-auto w-50">
                                </span>
                                    <span class="product-card__body">
                                    <span class="product-card__title text-center">
                                        <?php echo e($single_product->name); ?>

                                    </span>

                                    <span class="t-mt-10 d-block">
                                   <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                        <?php echo e(formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                                                                                                   == formatPrice(brandProductPrice($single_product->sellers)->max())
                                                                                                                                                   ? formatPrice(brandProductPrice($single_product->sellers)->min())
                                                                                                                                                   : formatPrice(brandProductPrice($single_product->sellers)->min()).
                                                                                                                                                   '-' .formatPrice(brandProductPrice($single_product->sellers)->max())); ?>

                                                                                                                                            </span>
                                                                                                                                            </span>

                                    </span>

                                </span>
                                </a>
                            </div>
                        </div>


                    <?php else: ?>

                        <div class="ps-carousel--nav owl-slider" data-owl-auto="true" data-owl-loop="true"
                             data-owl-speed="10000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="true"
                             data-owl-item="6" data-owl-item-xs="2" data-owl-item-sm="2" data-owl-item-md="3"
                             data-owl-item-lg="4" data-owl-item-xl="5" data-owl-duration="1000" data-owl-mousedrag="on">
                             <div class="row">
                            <?php $__currentLoopData = $related_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $related_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div class="col-md-3 t-mb-30">
                                        <a href="<?php echo e(route('single.product',[$related_product->sku,$related_product->slug])); ?>"
                                           class="product-card">
                                <span class="product-card__action d-flex flex-column align-items-center ">
                                    <span class="product-card__action-is product-card__action-view"
                                          onclick="forModal('<?php echo e(route('quick.view',$related_product->slug)); ?>', 'Product quick view')">
                                    <i class="fa fa-eye"></i>
                                    </span>
                                    <span class="product-card__action-is product-card__action-compare"
                                          onclick="addToCompare(<?php echo e($related_product->id); ?>)">
                                    <i class="fa fa-random"></i>
                                    </span>
                                    
                                    
                                    <?php if(auth()->guard()->check()): ?>
                                        <span class="product-card__action-is product-card__action-wishlist"
                                              onclick="addToWishlist(<?php echo e($related_product->id); ?>)">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                    <?php endif; ?>

                                    <?php if(auth()->guard()->guest()): ?>
                                        <span 
                                                                class="product-card__action-is product-card__action-wishlist wishlist"
                                                            
                                                                data-placement="top" 
                                                                data-title="Add to wishlist"
                                                                data-toggle="tooltip" 
                                                                data-product_name='<?php echo e($related_product->name); ?>' 
                                                                data-product_id='<?php echo e($related_product->id); ?>' 
                                                                data-product_sku='<?php echo e($related_product->sku); ?>' 
                                                                data-product_slug='<?php echo e($related_product->slug); ?>' 
                                                                data-product_image='<?php echo e(filePath($related_product->image)); ?>' 
                                                                data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                                                data-product_price='<?php echo e(formatPrice(brandProductPrice($related_product->sellers)->min())
                                                                                                == formatPrice(brandProductPrice($related_product->sellers)->max())
                                                                                                ? formatPrice(brandProductPrice($related_product->sellers)->min())
                                                                                                : formatPrice(brandProductPrice($related_product->sellers)->min()).
                                                                                                '-' .formatPrice(brandProductPrice($related_product->sellers)->max())); ?>'    
                                                                >
                                                                    <i class="fa fa-heart-o"></i>
                                                                </span>
                                    <?php endif; ?>


                                                                




                                </span>
                                            <span class="product-card__img-wrapper text-center w-50">
                                    <img src="<?php echo e(filePath($related_product->image)); ?>" alt="#<?php echo e($related_product->name); ?>"
                                         class="img-fluid mx-auto">
                                </span>
                                            <span class="product-card__body">
                                    <span class="product-card__title text-center">
                                       <?php echo e($related_product->name); ?>

                                    </span>


                                    <span class="t-mt-10 d-block text-center">

                                                                    <span class="product-card__discount-price t-mr-5">
                                                                        <?php echo e(formatPrice(brandProductPrice($related_product->sellers)->min())
                                                                           == formatPrice(brandProductPrice($related_product->sellers)->max())
                                                                           ? formatPrice(brandProductPrice($related_product->sellers)->min())
                                                                           : formatPrice(brandProductPrice($related_product->sellers)->min()).
                                                                           '-' .formatPrice(brandProductPrice($related_product->sellers)->max())); ?>

                                                                    </span>
                                                                    </span>



                                </span>
                                        </a>
                                    </div>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>

                        </div>
                    <?php endif; ?>


                </div>
            </div>

        </div>
    </div>

    
    <input id="variantUrl" value="<?php echo e(route('product.variant.seller')); ?>" type="hidden">
    <input id="sellerFound" value="<img src='<?php echo e(asset('shop-not-found.png')); ?>'>" type="hidden">
    <input id="title" value="Please select the product variant" type="hidden">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        "use strict"

        /*check have variant*/
        $(document).ready(function () {
            <?php if($units_array != null): ?>
            $('[data-toggle="tooltip"]').tooltip({delay: {"show": 500, "hide": 100}})
            $('#check_shop').removeAttr('href');
            $('.bookWithoutVariant').removeAttr('onclick');
            <?php else: ?>

            $('.bookWithoutVariant').removeAttr('data-title');

            $('[data-toggle="tooltip"]').tooltip('disable')
            $('#check_shop').attr('href', '#shops');
            // alert();
            <?php endif; ?>
        })

        //published the all
        $('input[type="radio"]').on('click',(function () {

            //radio check uncheck

            $('#check_shop').attr('href', '#shops');
            $('[data-toggle="tooltip"]').tooltip('disable')
            var previousValue = $(this).attr('previousValue');
            var name = $(this).attr('name');

            if (previousValue == 'checked') {
                $(this).removeAttr('checked');
                $(this).attr('previousValue', false);


            } else {
                $("input[name=" + name + "]:radio").attr('previousValue', false);
                $(this).attr('previousValue', 'checked');
            }


            var variants_id = [];
            <?php $__currentLoopData = $units_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            var v = $("input[name='<?php echo e($u); ?>']:checked").val();
            if (v != null) {
                variants_id.push(v);
            }
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            var url = $('#variantUrl').val();
            var pId = $('#productId').val();
            var sellerFound = $('#sellerFound').val();


            if (url != null && variants_id != null) {
                $.ajax({
                    url: url,
                    data: {id: variants_id, productId: pId},
                    method: "get",
                    success: function (result) {
                        $('.seller-div').empty();
                        if (result.data.length > 0) {
                            result.data.forEach(sellerShow);
                        } else {
                            $('.seller-div').append(sellerFound)
                        }
                    },
                });
            }

        }));

        /*seller show*/
        function sellerShow(item, index) {
            $(".seller-div").append('<div class="col-md-3">\n' +
            '                                    <aside class="widget widget_same-brand widget_vendor-shop ' + item.stock_out + '">\n' +
            '                                        <div class="widget__content widget_vendor-shop-content">\n' +
            '                                            <div class="ps-product">\n' +
            '                                                <div class="ps-product__thumbnail">\n' +
            '                                                            <a href="' + item.vendor_link + '">\n' +
            '                                                                <img\n' +
            '                                                                    src="' + item.shop_logo + '"\n' +
            '                                                                    class="rounded"\n' +
            '                                                                    alt="#$seller->user->vendor->shop_name">\n' +
            '                                                            </a></div>\n' +
            '                                                <div class="ps-product__container text-center">\n' +
            '                                                    <div class="ps-product__content">\n' +
            '                                                        <p class="ps-product__price sale mb-0">' + item.price_format + '</p>\n' +
            '                                                        <p class="ps-product__price sale mb-0">' + item.discount_text + '</p>\n' +
            '                                                        <p class="ps-product__price sale mb-0">' + item.extra_price_format + '</p>\n' +
            '                                                        <p class="ps-product__price sale mb-0">' + item.total_price_format + '</p>\n' +
            '                                                        <p class="ps-product__price">' + item.variant + '</p>\n' +
            '                                                    </div>\n' +
            '                                                    <?php if(auth()->guard()->check()): ?>\n' +
            '                                                        <a href="#!"\n' +
            '                                                           class="btn btn-primary m-2 p-3 fs-12 ' + item.display + '  addToCart-' + item.vendor_stock_id + '"\n' +
            '                                                           onclick="addToCart(' + item.vendor_stock_id + ')">Buy Now</a>\n' +
            '                                                        <a href="#!"\n' +
            '                                                           class="btn btn-danger m-2 p-3 fs-12 ' + item.reverse_display + '">Out Of Stock</a>\n' +
            '                                                    <?php endif; ?>\n' +
            '                                                    <?php if(auth()->guard()->guest()): ?>\n' +
                                                    //    todo::akash guest add to cart in available shop
            '                                                        <a href="#!"\n' +
            '                                                           class="btn btn-primary m-2 p-3 fs-12 ' + item.display + '  addToGuestCart-' + item.vendor_stock_id + '"\n' +
            '                                                           onclick="addToGuestCart(' + item.vendor_stock_id + ')">Buy Now</a>\n' +
            '                                                        <a href="#!"\n' +
            '                                                           class="btn btn-danger m-2 p-3 fs-12 ' + item.reverse_display + '">Out Of Stock</a>\n' +
            '                                                    <?php endif; ?>\n' +
            '                                                </div>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </aside>\n' +
            '                                </div>');
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/product/single_product.blade.php ENDPATH**/ ?>