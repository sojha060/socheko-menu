<script type="text/javascript">
    "use strict"
    function confirm_modal(delete_url) {
        jQuery('#confirm-delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('href', delete_url);
    }
</script>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body text-center">
                <p>Are you sure want to delete?</p>
                <div class="alert alert-default-danger">
                   <b>Careful!</b> <p>It may contain relational data. Deleting an item can cause your system error.</p>
                    <p>You must ensure that it does not have any relational data.</p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <a id="delete_link" type="submit" class="btn btn-danger">Delete</a>
                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/layouts/includes/delete.blade.php ENDPATH**/ ?>