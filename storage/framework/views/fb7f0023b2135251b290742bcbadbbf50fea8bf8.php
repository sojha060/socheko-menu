
<?php $__env->startSection('title'); ?> Product create
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', '@translate(Product create)'); ?>
<?php $__env->startSection('content'); ?>
    <div class="card m-2">
        <div class="card-header">
            <h2 class="card-title">Add new product</h2>
        </div>

        <div class="card-body">
            <form method="post" action="<?php echo e(route('admin.products.store')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div>
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="text" name="name"
                               placeholder="Enter product name" required>
                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>

                    
                    

                    <div class="form-group">
                        <label>Select parent category</label>
                        <select class="form-control select2 parent-cat" name="parent_id" required>
                            <option value=""></option>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>

                    <div class="form-group mb-5">
                        <label>Select child category</label>
                        <input  class="childUrl" type="hidden" value="<?php echo e(route('admin.child.index')); ?>"
                               required>
                        <select class="form-control select2 childCatShow" name="category_id">

                        </select>
                    </div>

                    <div class="form-group">
                        <label>Short Description</label>
                        <textarea class="textarea" placeholder="Short description"
                                  name="short_desc"></textarea>
                    </div>

                    <div class="form-group mb-5">
                        <label>Big Description</label>
                        <textarea class="textarea" placeholder="Short description"
                                  name="big_desc"></textarea>
                    </div>

                    <div class="form-group mb-5">
                        <label>Mobile View Description</label>
                        <textarea class="textarea" placeholder="Mobile View Description"
                                  name="mobile_desc"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Featured image</label>
                        <input name="image" class="form-control-file" type="file" required>
                    </div>

                    <div class="form-group">
                        <label>Video Provider</label>
                        <select class="form-control select2" name="provider">
                            <option value=""></option>
                            <option value="youtube">Youtube</option>
                            <option value="vimeo">Vimeo</option>
                        </select>
                    </div>

                    <div class="form-group mb-5">
                        <label>Promotion Video Url</label>
                        <input class="form-control" type="url" name="video_url"
                               placeholder="Enter promotion video url">
                    </div>

                    <div class="form-group">
                        <label>Meta title</label>
                        <input class="form-control" type="text" name="meta_title"
                               placeholder="Enter product meta title">
                    </div>

                    <div class="form-group">
                        <label>Meta Description</label>
                        <input class="form-control w-100" type="text" name="meta_desc"
                               placeholder="Enter product meta description">
                    </div>

                    <div class="form-group mb-5">
                        <label>Meta Image</label>
                        <input class="form-control-file" type="file" name="meta_image">
                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Tags</label>
                        <input id="tags-input" type="text" name="tags" value="" class="form-control w-100"
                               data-role="tagsinput">
                    </div>

                    <div class="form-group">
                        <label> Add Tax
                            <input class="add-tax" name="tax" type="checkbox" >
                        </label>
                    </div>

                    <div class="tax-input d-none">
                        <div class="form-group">
                            <input class="form-control tax-input-val" type="number" name="tax" min="0" step="0.01"
                                   placeholder="Enter tax %">
                        </div>
                    </div>

                    
                    <?php if(!vendorActive()): ?>
                        <div class="form-group">
                            <label>Price <small class="text-danger">*</small></label>

                            <input name="product_price" min="0" class="form-control" type="number" step="0.01" required
                                   placeholder="Product price">
                        </div>

                        <div class="form-group d-none">
                            <label>Purchase Price <small class="text-danger">*</small></label>
                            <input name="purchase_price" class="form-control" type="number"
                                   placeholder="Purchase product price">
                        </div>



                        <div class="form-group">
                            <label>
                                <input id="is_discount" name="is_discount" type="checkbox">
                                Has discount?
                            </label>
                        </div>


                        <div class="form-group d-none" id="discount_price">
                            <div class="row">
                                <div class="col-md-8">
                                    <label>Discounted Price</label>
                                    <input name="discount_price" class="form-control" type="number"
                                           placeholder="Discounted price">
                                </div>
                                <input type="hidden" name="discount_type" value="flat">
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="form-group">
                        <label>Add multiple product image</label>
                        <input name="images[]" class="form-control-file" type="file" multiple>

                    </div>

                    <div class="form-group">
                        <label> Add Variant
                            <input type="hidden" name="add_variant" value="off">
                            <input class="add-variant" name="add_variant" type="checkbox" value="on">
                        </label>
                    </div>




                    <div class="row d-none sr-variant">
                        
                        <strong class="text-info">If you select any variant you can't edit it, you can only add new variant</strong>
                        <div class="form-group col-12">
                            <label>Select variant</label>
                            <select class="form-control select2" name="units[]" multiple id="units">
                                <option value=""></option>
                                <?php $__currentLoopData = $variants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($variant->first()->unit); ?>"><?php echo e($variant->first()->unit); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <hr>
                        
                        <?php $__currentLoopData = $variants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $variant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="form-group col-12 <?php echo e($variant->first()->unit); ?> d-none">
                                <label><?php echo e($variant->first()->unit); ?></label>
                                <select class="form-control colorVariant" name="variant_id[]" multiple>
                                    <option value=""></option>
                                    <?php $__currentLoopData = $variant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $var): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($var->id); ?>" data-color="<?php echo e($var->code); ?>"
                                                data-name="<?php echo e($var->variant); ?>"><?php echo e($var->variant); ?></option>
                                        <?php echo e($var->unit); ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/product/create.blade.php ENDPATH**/ ?>