



<?php $__env->startSection('title'); ?><?php echo e($seller_store->shop_name); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<!-- breadcrumb:START -->

<!-- breadcrumb:END -->

<div class="ps-vendor-store">
    <div class="container">

        <!-- Newly added - Philip Basnet -->
        <div class="restro-bar">
            <div class="contain-left">
                <!-- Logo goes here -->
                <img id="restro-logo" src="<?php echo e(filePath($seller_store->shop_logo)); ?>" alt="<?php echo e($seller_store->name); ?>">
                <div class="restro-info">
                    <h1><?php echo e($seller_store->name); ?> <span class="restaurant-type">Restaurant Type:<?php echo e($seller_store->type); ?> </span>
                    </h1>
                    <!-- Rating vaues here. -->
                    <address>
                        <h4><?php echo e($seller_store->address); ?></h4>. <span>0% Positive (0 rating)</span>
                    </address>
                </div>
            </div>

            <?php if($seller_store->feature_photo): ?>

                <img class="cover-photo" src="<?php echo e(filePath($seller_store->feature_photo)); ?>" alt="<?php echo e($seller_store->name); ?>">

            <?php else: ?> 
                <img class="cover-photo" src="<?php echo e(asset('no-cover.jpg')); ?>" alt="<?php echo e($seller_store->name); ?>">

            <?php endif; ?> 
        </div>
        <div class="ps-section__container">
            <!-- SELLER PRODUCTS -->
            <div class="ps-section__left">
                <div class="ps-shopping ps-tab-root ps-custom">
                    <div class="ps-shopping__header">
                        <p><strong>Categories</strong></p>
                    </div>

                    <!-- Category Items Here -->
                    <div class="category-items">
                        <ul>
                            <!-- <li><a id="main-category" href="#">Fast Food</a></li> -->


                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                <?php
                                    $sub_categories = \App\Models\Category::select('*')
                                                            ->join('products','products.parent_id','=','categories.id')
                                                            ->whereIn('products.id',$products_array)
                                                            ->where('parent_category_id',$value->id)
                                                            ->get();

                                    // dd($sub_categories);
                                ?>



                                

                                <?php if($sub_categories->isEmpty()==false): ?>

                                <li>
                                    
                                    <a class="main-category" id="main-category"
                                   data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                       
                                    <?php echo e($value->name); ?></a></li>

                                

                                <div class="sub-category" style="display: none;" id="#collapseExample">
                                    <ul>
                                        <?php $__currentLoopData = $sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <a id="sub-category-item" href="#"><?php echo e($v->name); ?></a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                    </ul>
                                </div>
                                    
                                <?php endif; ?>


                                
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                           
                          
                        </ul>
                    </div>
                </div>

            </div>
            <div class="ps-shopping ps-tab-root">
                <div class="ps-shopping__header">
                    <p><strong> <?php echo e($products->count()); ?> </strong> Products found</p>
                    <div class="ps-shopping__actions">
                        <div class="d-none">
                            <select class="ps-select" data-placeholder="Sort Items">
                                <option>Sort by latest</option>
                                <option>Sort by popularity</option>
                                <option>Sort by average rating</option>
                                <option>Sort by price: low to high</option>
                                <option>Sort by price: high to low</option>
                            </select>
                        </div>
                        <!-- <div class="ps-shopping__view">
                                    <p>View</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                        <li><a href="#tab-2"><i class="icon-list4"></i></a></li>
                                    </ul>
                                </div> -->
                    </div>
                </div>

                
        

                <form class="ps-form--newsletter" id="search-item-food" action="<?php echo e(route('search.venodor.product')); ?>" method="get">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="ps-form__right">
                                <div class="form-group--nest">
                                    
                                    <input type="hidden" name="vendor_id" value="<?php echo e($seller_store->id); ?>">
                                    <input class="form-control" name="q" type="text"
                                    
                                    value="<?php echo e(@$q); ?>"
                                    id="myInput" placeholder="Search Food Items">
                                    <button type="submit" class="ps-btn">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                <div class="ps-tabs" id="myStore">

                    <div class="ps-tab active" id="tab-2">

                        <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                        <!-- TODO::OLD -->
                        <div class="ps-product ps-product--wide">
                            <div class="ps-product__thumbnail">
                                <a
                                    href="<?php echo e(route('single.product',[$product->product->sku,$product->product->slug])); ?>">
                                    <img id="item-image" src="<?php echo e(filePath($product->product->image)); ?>"
                                        alt="<?php echo e($product->product->name); ?>"></a>
                                <div class="ps-product__badge"><?php echo e($product->product->discount_percentage - 100); ?>

                                    %
                                </div>
                            </div>
                            <div class="ps-product__container">
                                <div class="ps-product__content"><a class="ps-product__title"
                                        href="<?php echo e(route('single.product',[$product->product->sku,$product->product->slug])); ?>">
                                        <?php echo e($product->product->name); ?>

                                    </a>

                                    <p>
                                        <?php echo Purify::clean($product->product->short_desc); ?>

                                    </p>
                                </div>
                                <div class="ps-product__shopping">
                                    <span class="t-mt-10 d-block">
                                        <span class="product-card__discount-price t-mr-5">
                                            <?php echo e(formatPrice(brandProductSalePrice($product->product->sellers)->min())
                                                                                        == formatPrice(brandProductSalePrice($product->product->sellers)->max())
                                                                                        ? formatPrice(brandProductSalePrice($product->product->sellers)->min())
                                                                                        : formatPrice(brandProductSalePrice($product->product->sellers)->min()).
                                                                                        '-' .formatPrice(brandProductSalePrice($product->product->sellers)->max())); ?>

                                        </span>
                                        <del class="product-card__price">
                                            <?php echo e(formatPrice(brandProductPrice($product->product->sellers)->min())
                                                                                       == formatPrice(brandProductPrice($product->product->sellers)->max())
                                                                                       ? formatPrice(brandProductPrice($product->product->sellers)->min())
                                                                                       : formatPrice(brandProductPrice($product->product->sellers)->min()).
                                                                                       '-' .formatPrice(brandProductPrice($product->product->sellers)->max())); ?>

                                        </del>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- TODO::OLD END-->

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                            <img src="<?php echo e(asset('no-product-found.png')); ?>" alt="#no-product-found">
                        </div>
                        <?php endif; ?>


                        <!-- TODO::OLD END-->

                        <div class="ps-pagination">
                            <?php echo e($products->links()); ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- SHOP:START -->

<!-- SHOP:END -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/seller/seller_shop.blade.php ENDPATH**/ ?>