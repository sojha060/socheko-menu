

<?php $__env->startSection('title'); ?> Track Order <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                    <li>Track Order</li>
                </ul>
            </div>
        </div>
    </div>


    <div class="ps-order-tracking">
            <div class="container">
                <div class="ps-section__header">
                    <h3>Order Tracking</h3>
                    <p>To track your order please enter your Order ID in the box below and press the "Track" Button. This was given to you on your receipt and in the confirmation email you should have received.</p>
                </div>
                <div class="ps-section__content">
                    <form class="ps-form--order-tracking" id="trackForm" action="javascript.void(0)" method="GET">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="url" value="<?php echo e(route('customer.track.order.number')); ?>">
                        <div class="form-group">
                            <label>Order ID</label>
                            <input class="form-control" id="order_number" value="<?php echo e($code ?? ''); ?>" name="order_number" required type="text" placeholder="Found in your order confimation email">
                        </div>
                        <div class="form-group">
                            <label>Billing Email</label>
                            <input class="form-control" id="email" name="email" value="<?php echo e(Auth::user()->email); ?>" required type="email" placeholder="Enter Your Email Address">
                        </div>
                        <div class="form-group">
                            <button class="ps-btn ps-btn--fullwidth" id="trackSubmit" onclick="trackOrder(this)" type="button">Track Your Order</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        

            <div class="container">
                <div id="loading" class="h2 text-center"></div>
                <div id="noResult" class="h2 text-center"></div>
                <div id="trackResult">
                    
                </div>
            </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/order/track_order.blade.php ENDPATH**/ ?>