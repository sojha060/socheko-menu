
<?php $__env->startSection('title'); ?> Sub Categories
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'All category'); ?>
<?php $__env->startSection('content'); ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title font-weight-bold"></h2>
                <h2 class="card-title"><span><a class="btn btn-sm btn-secondary" href="<?php echo e(route('parent.categories.index',[$category->parent->id,$category->parent->slug])); ?>"><i class="fa fa-reply"></i> </a></span> Parent category : <span class="font-weight-bold"><?php echo e($category->name); ?></span></h2>
            </div>
            <div class="float-right">
                <a href="#!"
                   onclick="forModal('<?php echo e(route('categories.create',[$category->id,$category->slug])); ?>', 'Add category');"
                   class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                    Add sub-category
                </a>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-striped table-bordered table-hover text-center datatable">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th class="text-left">Sub-category</th>
                    <?php if(vendorActive()): ?>
                    <th class="text-left">Commission</th>
                    <?php endif; ?>
                    <th>Icon class</th>
                    <th>Image</th>
                    <th>Trending</th>
                    <th>Publish</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e(($loop->index+1)); ?></td>
                        <td class="text-left"><?php echo e($sub->name); ?></td>
                        <?php if(vendorActive()): ?>
                        <td  class="text-left">
                            <?php if($sub->commission != null): ?>
                            <?php echo e($sub->commission->amount ?? ''); ?> <?php echo e($sub->commission->type == "percentage" ? '%':''); ?>

                            <?php else: ?>
                                Commission is not select
                            <?php endif; ?>

                        </td>
                        <?php endif; ?>
                        <td class="text-center">
                            <?php if($sub->icon != null): ?>
                                <i class="<?php echo e($sub->icon); ?>"></i>
                            <?php endif; ?>
                        </td>

                        <td>
                            <?php if($sub->image != null): ?>
                                <img src="<?php echo e(filePath($sub->image)); ?>" width="80" height="80"
                                     class="img-thumbnail" alt="<?php echo e($sub->name); ?>">
                            <?php endif; ?>
                        </td>
                        <td>
                            <div class="form-group">
                                <div
                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($sub->id); ?>"
                                           <?php echo e($sub->is_popular == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.popular')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="is_popular_<?php echo e($sub->id); ?>">
                                    <label class="custom-control-label" for="is_popular_<?php echo e($sub->id); ?>"></label>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($sub->id); ?>"
                                           <?php echo e($sub->is_published == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.published')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="is_published_<?php echo e($sub->id); ?>">
                                    <label class="custom-control-label" for="is_published_<?php echo e($sub->id); ?>"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle btn-sm"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="nav-link text-black"  href="#!" onclick="forModal('<?php echo e(route('categories.edit', [$sub->id,$sub->slug])); ?>', '<?php echo e($sub->name); ?>');">Edit</a></li>
                                    <li class="divider"></li>
                                    <li><a class="nav-link text-black" href="#!"
                                           onclick="confirm_modal('<?php echo e(route('categories.destroy', $sub->id)); ?>')">Delete</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="9"><h3 class="text-center">No Data Found</h3></td>
                    </tr>
                <?php endif; ?>
                </tbody>

            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/category/subIndex.blade.php ENDPATH**/ ?>