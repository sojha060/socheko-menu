

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?> Register <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                <li>My account</li>
            </ul>
        </div>
    </div>




    <div class="ps-my-account">
        <div class="container">
            <div class="ps-form--account ps-tab-root">
                <div class="ps-tab-list">
                    <span class="p-2 fs-28  font-weight-bold"><a href="<?php echo e(route('login')); ?>"
                                                                 class="<?php echo e(request()->is('login') ? 'color-active' : null); ?>">Login</a></span>
                    <span class="p-2 fs-28 font-weight-bold"><a href="<?php echo e(route('register')); ?>"
                                                                class="<?php echo e(request()->is('register') ? 'color-active' : null); ?>">Register</a></span>
                </div>
                <div class="card card-primary card-outline">
                    
                    <div class="m-4">
                    <?php if(Session::has('status')): ?>
                        <div class="alert alert-info text-center"><?php echo e(Session::get('status')); ?></div>
                    <?php endif; ?>
                    <?php if(Session::has('warning')): ?>
                        <div class="alert alert-info text-center"><?php echo e(Session::get('warning')); ?></div>
                    <?php endif; ?>
                    </div>
                    <div class="card-body" id="sign-in">
                        <div class="ps-form__content">
                            <form method="POST" action="<?php echo e(route('login')); ?>">
                                <?php echo csrf_field(); ?>
                                <h5>Log In Your Account</h5>
                                <div class="form-group">
                                    <input class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="email"
                                           name="email" placeholder="Email address" required>
                                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($message); ?></strong>
                                            </span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="form-group form-forgot">
                                    <input class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="password"
                                           name="password" placeholder="Password" required>
                                    <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($message); ?></strong>
                                            </span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    <a href="<?php echo e(route('password.request')); ?>">Forgot?</a>
                                </div>
                                <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control" type="checkbox" id="remember-me" name="remember">
                                        <label for="remember-me">Rememeber me</label>
                                    </div>
                                </div>
                                <div class="form-group submtit">
                                    <button class="ps-btn ps-btn--fullwidth" type="submit">Login</button>
                                </div>
                            </form>
                        </div>


                        <div class="ps-form__footer">
                            <p>Connect with:</p>
                            <ul class="ps-list--social">
                                
                                    <li><a class="facebook" href="<?php echo e(url('/auth/redirect/facebook')); ?>"><i
                                                    class="fa fa-facebook"></i></a></li>

                                    <li><a class="google" href="<?php echo e(url('/auth/redirect/google')); ?>"><i class="fa fa-google-plus"></i></a></li>
                                

                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/auth/login.blade.php ENDPATH**/ ?>