<div class="card-body">
    <form action="<?php echo e(route('categories.update')); ?>" method="post" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="id" value="<?php echo e($category->id); ?>">
        <input type="hidden" name="parent_id" value="<?php echo e($category->parent_category_id); ?>">
        <input type="hidden" name="image" value="<?php echo e($category->image); ?>">
        <input type="hidden" name="slug" value="<?php echo e($category->slug); ?>">
        <div class="form-group">
            <label>Name <span class="text-danger">*</span></label>
            <input class="form-control" name="name" placeholder="Name" required value="<?php echo e($category->name); ?>">
        </div>


        <div class="form-group">
            <label class="col-form-label text-md-right">Icon Class</label>
            <div class="custom-file">
                <input class="form-control" value="<?php echo e($category->icon); ?>" name="icon" type="text">
                <small>Want more icon ? <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">Font
                        Awesome</a></small>
            </div>
        </div>

        <?php if($category->image != null): ?>
            <img src="<?php echo e(filePath($category->image)); ?>" width="80" height="80" class="img-thumbnail">
        <?php endif; ?>
        <div class="form-group">
            <label class="col-form-label text-md-right">Image</label>
            <div class="">
                <input class="form-control-file sr-file" placeholder="Choose Image  only" name="newImage"
                       type="file">
                <small class="text-info">Upload file support png, jpg, svg format</small>
            </div>
        </div>
        <?php if(vendorActive()): ?>
            <div class="form-group">
                <label>Select commission</label>
                <select class="form-control select2 w-100" name="commission_id" required>
                    <option value="">Select commission</option>
                    <?php $__currentLoopData = $commissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option
                            value="<?php echo e($item->id); ?>" <?php echo e($category->commission_id == $item->id ? 'selected':null); ?>><?php echo e($item->amount); ?>

                            %
                        </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label>Meta Title</label>
            <input class="form-control" name="meta_title" value="<?php echo e($category->meta_title); ?>" type="text" max="100"
                   placeholder="Meta title">
            <small class="text-info">Google standard 100 characters</small>
        </div>

        <div class="form-group">
            <label>Meta Description</label>
            <input class="form-control form-control-lg" name="meta_desc" max="200" value="<?php echo e($category->meta_desc); ?>">
            <small class="text-info">Google standard 200 characters</small>
        </div>

        <div class="float-right">
            <button class="btn btn-primary float-right" type="submit">Update</button>
        </div>

    </form>
</div>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/category/edit.blade.php ENDPATH**/ ?>