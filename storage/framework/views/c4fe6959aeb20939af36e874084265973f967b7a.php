
<form class="border border-light p-5" action="<?php echo e(route('order.followup_comment', $followup)); ?>" method="POST"
      enctype="multipart/form-data">
    <?php echo csrf_field(); ?>


    <label for="textarea">Write a reason for follow up.</label>
    <textarea id="textarea" name="comment" class="form-control mb-4" placeholder="Customer did not pick up the call"
              required></textarea>

    <button class="btn btn-info btn-block my-4" type="submit">Submit</button>

</form>


<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/orders/followup_modal.blade.php ENDPATH**/ ?>