<?php if($paginator->hasPages()): ?>
<div class="ps-pagination">
    <ul class="pagination">

      
      <?php if($paginator->onFirstPage()): ?>
          
      <?php else: ?>
          <li><a href="<?php echo e($paginator->previousPageUrl()); ?>"  rel="prev" aria-label="<?php echo app('translator')->get('pagination.previous'); ?>"><i class="icon-chevron-left"></i>Previous Page</a></li>
      <?php endif; ?>


      
      <?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          
          <?php if(is_string($element)): ?>
              <li class="disabled" aria-disabled="true"><span><?php echo e($element); ?></span></li>
          <?php endif; ?>

          
          <?php if(is_array($element)): ?>
              <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($page == $paginator->currentPage()): ?>
                      <li class="active" aria-current="page"><a href="#"><?php echo e($page); ?></a></li>
                  <?php else: ?>
                      <li><a href="<?php echo e($url); ?>"><?php echo e($page); ?></a></li>
                  <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      
      <?php if($paginator->hasMorePages()): ?>
          <li><a href="<?php echo e($paginator->nextPageUrl()); ?>" rel="next" aria-label="<?php echo app('translator')->get('pagination.next'); ?>">Next Page<i class="icon-chevron-right"></i></a></li>
      <?php else: ?>
          
      <?php endif; ?>


    </ul>
</div>
<?php endif; ?>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/include/pagination/paginate_shop.blade.php ENDPATH**/ ?>