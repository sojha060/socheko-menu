<div class="ps-top-categories">
    <div class="container">
        <div class="ps-section__header">
            <h3>TOP CATEGORIES OF THE MONTH</h3>
        </div>
        <div class="ps-section__content"></div>
        <div class="row align-content-lg-stretch">
            <?php $__empty_1 = true; $__currentLoopData = categories(10, null); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <?php if($home_category->top == 1): ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="ps-block--category-2 br-10" data-mh="categories">
                      <div class="ps-block__thumbnail ml-4">
                          <img src="<?php echo e(filePath($home_category->image)); ?>" height="50px" alt="" class="rounded-left"></div>
                      <div class="ps-block__content">
                          <h4><?php echo e($home_category->name); ?></h4>
                          <ul>
                            <?php
                                $category_limit = 0;
                            ?>
                            <?php $__currentLoopData = $home_category->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_Cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php $__currentLoopData = $parent_Cat->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <input type="hidden" value="<?php echo e($category_limit++); ?>">
                                <li><a href="<?php echo e(route('category.shop',$sub_cat->slug)); ?>"><?php echo e($sub_cat->name); ?></a></li>
                               
                                <?php if($category_limit == 13): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                               
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                              <?php if($category_limit == 13): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </ul>
                      </div>
                  </div>
              </div>
            <?php endif; ?>
            
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <img src="<?php echo e(asset('no-category-found.jpg')); ?>" class="img-fluid" alt="#no-category-found">
           <?php endif; ?>
        </div>
    </div>
</div>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/widgets/section/top-categories.blade.php ENDPATH**/ ?>