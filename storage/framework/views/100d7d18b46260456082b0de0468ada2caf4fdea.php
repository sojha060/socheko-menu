
<?php $__env->startSection('title'); ?> Special Offers
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'All Special Offers'); ?>
<?php $__env->startSection('content'); ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">Special Offer List</h2>
            </div>
            <div class="float-right">
                <div class="row text-right">
                    <div class="col-12">
                        <form action="<?php echo e(route('seller.campaign.search')); ?>" method="get">
                            <?php echo csrf_field(); ?>
                            <div class="input-group">
                                <input type="text" name="search" class="form-control col-12"
                                       placeholder="Title / Offer" value="<?php echo e(Request::get('search')); ?>">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover text-center table-sm">
                    <thead>
                    <tr>
                        <th>S/L</th>
                        <th>Title</th>
                        <th>Offer</th>
                        <th>Duration</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__empty_1 = true; $__currentLoopData = $campaigns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <td><?php echo e(($loop->index+1) + ($campaigns->currentPage() - 1)*$campaigns->perPage()); ?></td>
                            <td><?php echo e($item->title); ?></td>


                            <td><?php echo e($item->offer); ?>%</td>
                            <td>(<?php echo e(\Carbon\Carbon::parse($item->start_from)->format('d F, Y')); ?>)  -  (<?php echo e(\Carbon\Carbon::parse($item->end_at)->format('d F, Y')); ?>)</td>

                            <td>
                                <a href="<?php echo e(route('seller.campaign.products.index',$item->slug)); ?>" class="btn btn-sm btn-outline-secondary"><i class="fa fa-plus text-success"></i> Your Products <i class="fa fa-times text-danger"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="6"><h3 class="text-center" >No Data Found</h3></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                    <div class="float-left">
                        <?php echo e($campaigns->links()); ?>

                    </div>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/campaign/index.blade.php ENDPATH**/ ?>