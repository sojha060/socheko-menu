<div class="card-body">
    <h3><b>Your Current Balance is : <?php echo e(formatPrice($vendor->balance)); ?></b></h3>
    <form action="<?php echo e(route('payments.store')); ?>" method="post" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="status" value="Request">
        <div class="form-group">
            <label>Withdrawal Amount <span class="text-danger">*</span></label>
            <input class="form-control" type="number" min="10" max="<?php echo e($vendor->balance); ?>" name="amount" required>
        </div>
        <div class="form-group">
            <label>Payment Process <span class="text-danger">*</span></label>
            <select class="form-control lang select2" name="process" required>

                <?php if(isset($check_account->account_number) || isset($check_account->paypal_acc_name) || isset($check_account->stripe_card_number)): ?>
                    <option value="">Select The Payment Method</option>
                <?php else: ?>
                    <option value="">Setup Your Account First</option>
                <?php endif; ?>

                <?php if(isset($check_account->account_number)): ?>
                    <option value="Bank">Bank</option>
                <?php endif; ?>
                
                <?php if(isset($check_account->paypal_acc_name)): ?>
                    <option value="Paypal">Pay Pal</option>
                <?php endif; ?>
                
                <?php if(isset($check_account->stripe_card_number)): ?>
                    <option value="Stripe">Stripe</option>
                <?php endif; ?>

            </select>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="float-right">
            <button class="btn btn-primary float-right" type="submit">Save</button>
        </div>

    </form>
</div>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/payment/create.blade.php ENDPATH**/ ?>