
<?php $__env->startSection('title'); ?> Category Group
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'All Category'); ?>
<?php $__env->startSection('content'); ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">Category group list</h2>
            </div>

         
            <div class="float-right">
                <div class="row justify-content-end">
                    <div class="col">
                        <form method="get" action="">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control col-12"
                                       placeholder="Search group" value="<?php echo e(Request::get('search')); ?>">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col">
                        <a href="#!"
                           onclick="forModal('<?php echo e(route('group.categories.create')); ?>', 'Add Category Group');"
                           class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            Add new group
                        </a>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="card-body">
            <table class="table table-striped table-bordered table-hover text-center">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th class="text-left">Group name</th>
                    <th class="text-left">Parent Category List</th>
                    <th>Icon class</th>
                    <th>Image</th>
                    <th>Popular</th>
                    <th>Top</th>
                    <th>Publish</th>
                    <th>View</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  
                <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e(($loop->index+1) + ($categories->currentPage() - 1)*$categories->perPage()); ?></td>
                        <td class="text-left"><?php echo e($item->name); ?></td>
                        <td class="text-left">
                            <?php $__empty_2 = true; $__currentLoopData = $item->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                               <span class="badge badge-dark"><a class="text-white" href="<?php echo e(route('child.categories.index',[$cat->id,$cat->slug])); ?>"><?php echo e($cat->name); ?></a></span><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                N/A
                            <?php endif; ?>
                        </td>

                        <td class="text-center">
                            <?php if($item->icon != null): ?>
                                <i class="<?php echo e($item->icon); ?>"></i>
                            <?php endif; ?>
                        </td>

                        <td>
                            <?php if($item->image != null): ?>
                                <img src="<?php echo e(filePath($item->image)); ?>" width="80" height="80"
                                     class="img-thumbnail" alt="<?php echo e($item->name); ?>">
                            <?php endif; ?>
                        </td>

                        <td>
                            <div class="form-group">
                                <div
                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($item->id); ?>"
                                           <?php echo e($item->is_popular == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.popular')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="is_popular_<?php echo e($item->id); ?>">
                                    <label class="custom-control-label" for="is_popular_<?php echo e($item->id); ?>"></label>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="form-group">
                                <div
                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($item->id); ?>"
                                           <?php echo e($item->top == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.top')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="top_<?php echo e($item->id); ?>">
                                    <label class="custom-control-label" for="top_<?php echo e($item->id); ?>"></label>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="form-group">
                                <div
                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($item->id); ?>"
                                           <?php echo e($item->is_published == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.published')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="is_published_<?php echo e($item->id); ?>">
                                    <label class="custom-control-label" for="is_published_<?php echo e($item->id); ?>"></label>
                                </div>
                            </div>
                        </td>

                        <td class="text-center">
                            <a href="<?php echo e(route('parent.categories.index',[$item->id,$item->slug])); ?>" class="btn btn-sm btn-warning w-75">Parent categories</a>
                        </td>

                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle btn-sm"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#!" class="nav-link text-black"
                                           onclick="forModal('<?php echo e(route('group.categories.edit', $item->id)); ?>', 'Edit category group');">Edit</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="#!"
                                           onclick="confirm_modal('<?php echo e(route('categories.destroy', $item->id)); ?>')" class="nav-link text-black">Delete</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="9"><h3 class="text-center">No Data Found</h3></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>

         
        </div>
    </div>

    <div class="d-flex justify-content-center mt-3">
        <?php echo e($categories->links()); ?>

    </div>
    
    <?php if(@$sellerRequest != null && vendorActive()): ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">Requested sub-category list</h2>
            </div>
        </div>
       
        <div class="card-body">
            <table class="table table-striped table-bordered table-hover text-left">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th>Requested sub-category name</th>
                    <th>Category Group</th>
                    <th>Parent category</th>
                    <th>Requested by</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = @$sellerRequest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
               
                    <tr>
                        <td><?php echo e(@$loop->index+1); ?></td>
                      
                        <td><?php echo e(@$item->name); ?></td>
                      
                        <td>

                            <?php
                                $category = \App\Models\Category::where('id',@$item->parent->parent_category_id)->first();
                            ?>
                           <?php echo e(@$category->name); ?>

                        </td>
                     
                        <td>
                           <?php echo e(@$item->parent->name); ?>

                         </td>
                        <td>
                            <?php echo e(@$item->creator->name); ?>

                        </td>
                       
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle btn-sm"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#!" class="nav-link text-success"
                                           onclick="forModal('<?php echo e(route('categories.edit', [@$item->id,@$item->slug])); ?>', '<?php echo e(@$item->name); ?>');">Approve</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="#!"
                                           onclick="confirm_modal('<?php echo e(route('categories.destroy', @$item->id)); ?>')" class="nav-link text-danger">Decline</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="9"><h3 class="text-center">No Data Found</h3></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

       
    </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/category/index.blade.php ENDPATH**/ ?>