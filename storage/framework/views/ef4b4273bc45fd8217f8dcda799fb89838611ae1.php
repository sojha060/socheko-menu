


<?php $__env->startSection('title'); ?>Category Products <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                <li>Category Products</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--shop mt-3">
        <div class="ps-container">
            <div class="ps-shop-banner d-none">
                <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true"
                     data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1"
                     data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1"
                     data-owl-duration="1000" data-owl-mousedrag="on">
                    <?php $__currentLoopData = categories(10, null); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $home_category->promotionBanner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e($banner->link); ?>"><img src="<?php echo e(filePath($banner->image)); ?>" alt=""></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            
            <div class="ps-section__content d-none">
                <div class="ps-block--categories-tabs ps-tab-root">
                    <div class="ps-tabs">
                        <div class="ps-tabs">
                            <div class="ps-tab active">
                                <div class="ps-block__item">
                                    <?php $__empty_1 = true; $__currentLoopData = brands(16); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                        <a href="shop-default.html">
                                            <?php if(empty($brand->logo)): ?>
                                                <img src="<?php echo e(asset('vendor-store.jpg')); ?>" class="rounded"
                                                     alt="#<?php echo e($brand->name); ?>">
                                            <?php else: ?>
                                                <img src="<?php echo e(filePath($brand->logo)); ?>" class="rounded"
                                                     alt="#<?php echo e($brand->name); ?>">
                                            <?php endif; ?>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                        No Brand Found
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ps-shop-categories d-none">
                <div class="row align-content-lg-stretch">
                    <?php $__currentLoopData = categories(10, null); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--category-2" data-mh="categories">
                                <div class="ps-block__thumbnail"><img src="<?php echo e(filePath($home_category->image)); ?>"
                                                                      alt=""></div>
                                <div class="ps-block__content">
                                    <h4><?php echo e($home_category->name); ?></h4>
                                    <ul>
                                        <?php $__currentLoopData = $home_category->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_Cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $parent_Cat->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <a href="<?php echo e(route('category.shop',$sub_cat->slug)); ?>"><?php echo e($sub_cat->name); ?></a>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="ps-layout--shop">
                <div class="ps-layout__left">
                    <aside class="widget widget_shop">
                        <h4 class="widget-title">Categories</h4>
                        <ul class="ps-list--categories">

                            <?php $__currentLoopData = categories(500, null); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="current-menu-item menu-item-has-children"><a
                                            href="<?php echo e(route('category.shop', $home_category->slug)); ?>"><?php echo e($home_category->name); ?></a><span
                                            class="sub-toggle <?php echo e($parent_slug == $home_category->slug ? 'active' : null); ?>"><i
                                                class="fa fa-angle-down"></i></span>
                                    <ul class="sub-menu"
                                        style="display: <?php echo e($parent_slug == $home_category->slug ? 'block' : null); ?>">
                                        <?php $__currentLoopData = $home_category->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_Cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $parent_Cat->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="current-menu-item "><a
                                                            href="<?php echo e(route('category.shop',$sub_cat->slug)); ?>"><?php echo e($sub_cat->name); ?>

                                                        (<?php echo e(App\Models\Product::where('category_id', $sub_cat->id)->count()); ?>

                                                        )</a>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </aside>
                </div>
                <div class="ps-layout__right">
                    <div class="ps-shopping ps-tab-root">
                        <div class="ps-shopping__header">
                            <p><strong> <?php echo e($total_product); ?></strong> Products found</p>
                            <div class="ps-shopping__actions">
                                <div class="d-none">
                                    <select class="ps-select d-none" data-placeholder="Sort Items">
                                        <option>Sort by latest</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by average rating</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                                <div class="ps-shopping__view">
                                    <p>View</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                        <li><a href="#tab-2"><i class="icon-list4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ps-tabs">
                            <div class="ps-tab active" id="tab-1">
                                <div class="ps-shopping-product">
                                    <div class="row">
                                        <?php $__currentLoopData = $category_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $category_product->subCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <div class="col-md-3 col-xl-2 t-mb-30">
                                                    <a href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>"
                                                       class="product-card">
                                                                <span class="product-card__action d-flex flex-column align-items-center ">
                                                                    <span class="product-card__action-is product-card__action-view"
                                                                          onclick="forModal('<?php echo e(route('quick.view',$product->slug)); ?>', 'Product quick view')">
                                                                    <i class="fa fa-eye"></i>
                                                                    </span>
                                                                    <span class="product-card__action-is product-card__action-compare"
                                                                          onclick="addToCompare(<?php echo e($product->id); ?>)">
                                                                    <i class="fa fa-random"></i>
                                                                    </span>
                                                                    <?php if(auth()->guard()->check()): ?>
                                                                        <span class="product-card__action-is product-card__action-wishlist"
                                                                              onclick="addToWishlist(<?php echo e($product->id); ?>)">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                                                    <?php endif; ?>

                                                                    <?php if(auth()->guard()->guest()): ?>
                                                                        <span 
                                                                class="product-card__action-is product-card__action-wishlist wishlist"
                                                                data-placement="top" 
                                                                data-title="Add to wishlist"
                                                                data-toggle="tooltip" 
                                                                data-product_name='<?php echo e($product->name); ?>' 
                                                                data-product_id='<?php echo e($product->id); ?>' 
                                                                data-product_sku='<?php echo e($product->sku); ?>' 
                                                                data-product_slug='<?php echo e($product->slug); ?>' 
                                                                data-product_image='<?php echo e(filePath($product->image)); ?>' 
                                                                data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                                                data-product_price='<?php echo e(formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                == formatPrice(brandProductPrice($product->sellers)->max())
                                                                                                ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                                                '-' .formatPrice(brandProductPrice($product->sellers)->max())); ?>'    
                                                                >
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                                                    <?php endif; ?>


                                                                    

                                                                </span>
                                                        <span class="product-card__img-wrapper">
                                                                    <img src="<?php echo e(filePath($product->image)); ?>"
                                                                         alt="manyvendor" class="img-fluid mx-auto">
                                                                </span>
                                                        <span class="product-card__body">
                                                                    <span class="product-card__title">
                                                                        <?php echo e($product->name); ?>

                                                                    </span>


                                                                    <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                                                               <?php echo e(formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  == formatPrice(brandProductPrice($product->sellers)->max())
                                                                                                                  ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                                                                  '-' .formatPrice(brandProductPrice($product->sellers)->max())); ?>

                                                                                                           </span>
                                                                                                           </span>

                                                                    </span>

                                                    </a>
                                                </div>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php $__currentLoopData = $category_product->frontCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $__currentLoopData = $cat_product->CategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_two): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <input type="hidden" value="<?php echo e($total_product++); ?>">

                                                    <div class="col-md-3 col-xl-2 t-mb-30">
                                                        <a href="<?php echo e(route('single.product',[$product_two->sku,$product_two->slug])); ?>"
                                                           class="product-card">
                                                                <span class="product-card__action d-flex flex-column align-items-center ">
                                                                    <span class="product-card__action-is product-card__action-view"
                                                                          onclick="forModal('<?php echo e(route('quick.view',$product_two->slug)); ?>', 'Product quick view')">
                                                                    <i class="fa fa-eye"></i>
                                                                    </span>
                                                                    <span class="product-card__action-is product-card__action-compare"
                                                                          onclick="addToCompare(<?php echo e($product_two->id); ?>)">
                                                                    <i class="fa fa-random"></i>
                                                                    </span>
                                                                    <?php if(auth()->guard()->check()): ?>
                                                                        <span class="product-card__action-is product-card__action-wishlist"
                                                                              onclick="addToWishlist(<?php echo e($product_two->id); ?>)">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                                                    <?php endif; ?>

                                                                    <?php if(auth()->guard()->guest()): ?>
                                                                        <span 
                                                                class="product-card__action-is product-card__action-wishlist wishlist"
                                                                data-placement="top" 
                                                                data-title="Add to wishlist"
                                                                data-toggle="tooltip" 
                                                                data-product_name='<?php echo e($product_two->name); ?>' 
                                                                data-product_id='<?php echo e($product_two->id); ?>' 
                                                                data-product_sku='<?php echo e($product_two->sku); ?>' 
                                                                data-product_slug='<?php echo e($product_two->slug); ?>' 
                                                                data-product_image='<?php echo e(filePath($product_two->image)); ?>' 
                                                                data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                                                data-product_price='<?php echo e(formatPrice(brandProductPrice($product_two->sellers)->min())
                                                                                                == formatPrice(brandProductPrice($product_two->sellers)->max())
                                                                                                ? formatPrice(brandProductPrice($product_two->sellers)->min())
                                                                                                : formatPrice(brandProductPrice($product_two->sellers)->min()).
                                                                                                '-' .formatPrice(brandProductPrice($product_two->sellers)->max())); ?>'    
                                                                >
                                                                    <i class="fa fa-heart-o"></i>
                                                                    </span>
                                                                    <?php endif; ?>


                                                                    


                                                                </span>
                                                            <span class="product-card__img-wrapper">
                                                                    <img src="<?php echo e(filePath($product_two->image)); ?>"
                                                                         alt="manyvendor" class="img-fluid mx-auto">
                                                                </span>
                                                            <span class="product-card__body">
                                                                    <span class="product-card__title">
                                                                        <?php echo e($product_two->name); ?>

                                                                    </span>


                                                                    <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                                                               <?php echo e(formatPrice(brandProductPrice($product_two->sellers)->min())
                                                                                                                  == formatPrice(brandProductPrice($product_two->sellers)->max())
                                                                                                                  ? formatPrice(brandProductPrice($product_two->sellers)->min())
                                                                                                                  : formatPrice(brandProductPrice($product_two->sellers)->min()).
                                                                                                                  '-' .formatPrice(brandProductPrice($product_two->sellers)->max())); ?>

                                                                                                           </span>
                                                                                                           </span>

                                                            </span>

                                                        </a>
                                                    </div>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </div>
                                </div>


                            </div>
                            <div class="ps-tab" id="tab-2">
                                <div class="ps-shopping-product">
                                    <?php $__currentLoopData = $category_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $category_product->subCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                            <div class="ps-product ps-product--wide">
                                                <div class="ps-product__thumbnail"><a
                                                            href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>">
                                                        <img src="<?php echo e(filePath($product->image)); ?>" class="rounded"
                                                             alt="#<?php echo e($product->name); ?>"></a>
                                                </div>
                                                <div class="ps-product__container">
                                                    <div class="ps-product__content"><a class="ps-product__title"
                                                                                        href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>"><?php echo e($product->name); ?></a>

                                                        <?php echo Purify::clean($product->short_desc); ?>

                                                    </div>
                                                    <div class="ps-product__shopping">
                                                               <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                                                               <?php echo e(formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  == formatPrice(brandProductPrice($product->sellers)->max())
                                                                                                                  ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                                                                  '-' .formatPrice(brandProductPrice($product->sellers)->max())); ?>

                                                                                                           </span>
                                                                                                           </span>


                                                        <a class="ps-btn"
                                                           href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>">Buy
                                                            Now</a>
                                                        <ul class="ps-product__actions">

                                                            <li>
                                                                <?php if(auth()->guard()->check()): ?>
                                                                    <a data-toggle="tooltip" data-placement="top"
                                                                       title="Add to Whishlist" href="#!"
                                                                       onclick="addToWishlist(<?php echo e($product->id); ?>)"><i
                                                                                class="icon-heart"></i></a>
                                                                <?php endif; ?>
                                                                <?php if(auth()->guard()->guest()): ?>
                                                                    <a 
                                                                class="wishlist"
                                                                data-placement="top" 
                                                                data-title="Add to wishlist"
                                                                data-toggle="tooltip" 
                                                                data-product_name='<?php echo e($product->name); ?>' 
                                                                data-product_id='<?php echo e($product->id); ?>' 
                                                                data-product_sku='<?php echo e($product->sku); ?>' 
                                                                data-product_slug='<?php echo e($product->slug); ?>' 
                                                                data-product_image='<?php echo e(filePath($product->image)); ?>' 
                                                                data-app_url='<?php echo e(env('APP_URL')); ?>' 
                                                                data-product_price='<?php echo e(formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                == formatPrice(brandProductPrice($product->sellers)->max())
                                                                                                ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                                                '-' .formatPrice(brandProductPrice($product->sellers)->max())); ?>'    
                                                                >
                                                                    <i class="fa fa-heart-o"></i>
                                                                </a>
                                                                <?php endif; ?>


                                                                


                                                            </li>
                                                            <li>
                                                                <a href="#!" onclick="addToCompare(<?php echo e($product->id); ?>)"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Compare"><i class="fa fa-random"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>


                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        <?php $__currentLoopData = $category_product->frontCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $cat_product->CategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <div class="ps-product ps-product--wide">
                                                        <div class="ps-product__thumbnail"><a
                                                                    href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>">
                                                                <img src="<?php echo e(filePath($product->image)); ?>" class="rounded"
                                                                     alt="#<?php echo e($product->name); ?>"></a>
                                                        </div>
                                                        <div class="ps-product__container">
                                                            <div class="ps-product__content"><a class="ps-product__title"
                                                                                                href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>"><?php echo e($product->name); ?></a>

                                                                <?php echo Purify::clean($product->short_desc); ?>

                                                            </div>
                                                            <div class="ps-product__shopping">
                                                               <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                                                              <?php echo e(formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  == formatPrice(brandProductPrice($product->sellers)->max())
                                                                                                                  ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                                                                  : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                                                                  '-' .formatPrice(brandProductPrice($product->sellers)->max())); ?>

                                                                                                           </span>
                                                                                                           </span>


                                                                <a class="ps-btn"
                                                                   href="<?php echo e(route('single.product',[$product->sku,$product->slug])); ?>">Buy
                                                                    Now</a>
                                                                <ul class="ps-product__actions">

                                                                    <li>
                                                                        <?php if(auth()->guard()->check()): ?>
                                                                            <a data-toggle="tooltip" data-placement="top"
                                                                               title="Add to Whishlist" href="#!"
                                                                               onclick="addToWishlist(<?php echo e($product->id); ?>)"><i
                                                                                        class="icon-heart"></i></a>
                                                                        <?php endif; ?>
                                                                        <?php if(auth()->guard()->guest()): ?>
                                                                            <a data-toggle="tooltip" data-placement="top"
                                                                               title="Add to Whishlist"
                                                                               href="<?php echo e(route('login-redirect')); ?>?url=<?php echo e(url()->current()); ?>"><i
                                                                                        class="icon-heart"></i></a>
                                                                        <?php endif; ?>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#!" onclick="addToCompare(<?php echo e($product->id); ?>)"
                                                                           data-toggle="tooltip" data-placement="top"
                                                                           title="Compare"><i class="fa fa-random"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal d-none" id="shop-filter-lastest" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="list-group d-none">
                                <a class="list-group-item list-group-item-action" href="#">Sort
                                    by</a><a class="list-group-item list-group-item-action" href="#">Sort by average
                                    rating</a><a class="list-group-item list-group-item-action" href="#">Sort by
                                    latest</a><a class="list-group-item list-group-item-action" href="#">Sort by price:
                                    low to high</a><a class="list-group-item list-group-item-action" href="#">Sort by
                                    price: high to low</a><a class="list-group-item list-group-item-action text-center"
                                                             href="#" data-dismiss="modal"><strong>Close</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/shop/category_shop.blade.php ENDPATH**/ ?>