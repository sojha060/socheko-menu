

<?php $__env->startSection('title'); ?>Your order <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                    <li><a href="<?php echo e(route('customer.orders')); ?>">Your order</a></li>
                    <li><?php echo e(Auth::user()->name); ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard pro">
        <div class="container">
            <div class="ps-section__header">
                <h3>Customer Dashboard</h3>
            </div>
            <div class="ps-section__content">
                <ul class="ps-section__links">
                    <li class="active"><a href="<?php echo e(route('customer.orders')); ?>">Your Order</a></li>
                    <li><a href="<?php echo e(route('customer.index')); ?>">Your Profile</a></li>
                    <?php if(affiliateRoute() && affiliateActive()): ?>
                    <li><a href="<?php echo e(route('customers.affiliate.registration')); ?>">Affiliate Marketing</a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo e(route('logout')); ?>"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Sign Out
                        </a>

                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </li>
                </ul>
            </div>


            
            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <figure class="ps-block--vendor-status">
                        <figcaption>Your Orders(<?php echo e($orders->count()); ?>)</figcaption>

                        <table class="table ps-table ps-table--vendor">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Type</th>
                                <th>Totals</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__empty_1 = true; $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo e(route('customer.order_details', $order->order_number)); ?>"><?php echo e($order->order_number); ?></a>
                                        <p><?php echo e($order->created_at->format('M d, Y')); ?></p>
                                    </td>

                                    <td>
                                        <p><?php echo e($order->payment_type); ?></p>
                                    </td>
                                    <td>
                                        <p><?php echo e(formatPrice($order->pay_amount)); ?></p>
                                    </td>
                                    <td>
                                        <a href="<?php echo e(route('customer.order_details', $order->order_number)); ?>"
                                           class="btn btn-warning text-dark">Details</a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <tr class="text-center">
                                    <td colspan="4">
                                        No Order Found
                                    </td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </figure>
                </div>

            </div>
            
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/order/your_order.blade.php ENDPATH**/ ?>