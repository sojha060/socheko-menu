
<?php $__env->startSection('title'); ?> Seller Earning
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'Admin Earning'); ?>
<?php $__env->startSection('content'); ?>

    <!-- BAR CHART -->
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                Seller Earning</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="chart">
                <canvas class="admin-single-earning-chart"></canvas>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <hr>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">Seller Earning list</h2>
            </div>
            <div class="float-right">
                <div class="row justify-content-end">
                    <div class="col">
                        <form method="get" action="">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control col-12"
                                       placeholder="Search by  booking code"
                                       value="<?php echo e(Request::get('search')); ?>">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="btn btn-secondary px-5">
                        <div class="my-auto fs-16"><?php echo e(formatPrice($total_earning)); ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-striped table-bordered table-hover text-left">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th>Booking Code</th>
                    <th>Product Name</th>
                    <th>Total amount</th>
                    <th>Admin commission</th>
                    <th>Earned</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $earning; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e(($loop->index+1) + ($earning->currentPage() - 1)*$earning->perPage()); ?></td>
                        <td>#<?php echo e($item->booking_code); ?></td>
                        <td><?php echo e($item->product->name); ?></td>
                        <td><?php echo e(formatPrice($item->price)); ?></td>
                        <td><?php echo e(formatPrice($item->commission_pay)); ?></td>
                        <td><?php echo e(formatPrice($item->get_amount)); ?></td>
                        <td><?php echo e(date('d-M-Y',strtotime($item->created_at))); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="9"><h3 class="text-center">No Data Found</h3></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex justify-content-center mt-3">
        <?php echo e($earning->links()); ?>

    </div>



<?php $__env->stopSection(); ?>
<?php
/*get month*/
$months = array();
$labels = array();
for ($i = 1; $i <= 12; $i++) {
    $m = date("M", mktime(0, 0, 0, $i, 1, date('Y')));
    array_push($months, $m);
    array_push($labels, date('F', mktime(0, 0, 0, $i, 1, date("Y"))));
    if (date('M') == $m) {
        break;
    }
}

$earnings = array();
foreach ($months as $month) {
    $start_month = \Carbon\Carbon::parse($month)->startOfMonth()->toDateTimeString();
    $end_month = \Carbon\Carbon::parse($month)->endOfMonth()->toDateTimeString();
    $earning = \App\Models\SellerEarning::whereBetween('created_at', [$start_month, $end_month])->get()->sum('get_amount');
    array_push($earnings, $earning);
}
?>

<?php $__env->startSection('script'); ?>
    <script>
        "use strict"
        $(document).ready(function () {

            var areaChartData = {
                labels: [<?php $__currentLoopData = $labels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $l): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>'<?php echo e($l); ?>',<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>],
                datasets: [
                    {
                        label: 'This year seller earning',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: [<?php $__currentLoopData = $earnings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $earning): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($earning); ?>,<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>]
                    },
                ]
            }

            //-------------
            //- BAR CHART -
            //-------------
            var barChartCanvas = $('.admin-single-earning-chart').get(0).getContext('2d')
            var barChartData = jQuery.extend(true, {}, areaChartData)
            var temp = areaChartData.datasets[0]
            barChartData.datasets[0] = temp


            var barChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false
            }

            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            })
            /* END admin single earning chart BAR CHART */
        })
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/earning.blade.php ENDPATH**/ ?>