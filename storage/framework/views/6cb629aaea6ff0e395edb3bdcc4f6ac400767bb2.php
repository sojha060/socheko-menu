

<?php $__env->startSection('title'); ?> Shopping Cart<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="ps-page--simple">
  <div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
            <li>Shopping Cart</li>
        </ul>
    </div>
  </div>
  <div class="ps-section--shopping ps-shopping-cart">
      <div class="container">
          <div class="ps-section__header">
              <h1>Shopping Cart</h1>
          </div>
          <div class="ps-section__content">
              <div class="table-responsive">
                  <table class="table ps-table--shopping-cart text-center">
                      <thead>
                          <tr>
                              <th>Product name</th>
                              <th>PRICE</th>
                              <th>QUANTITY</th>
                              <th>TOTAL</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody class="guest-cart-blade">
                            
                      </tbody>
                  </table>
              </div>
             </div>
          <div class="ps-section__footer" data-select2-id="6">
              <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 offset-md-8">
                      <div class="ps-block--shopping-total">
                          <div class="ps-block__content">
                            <h3 class="d-flex justify-content-between">
                                <span class="text-dark">Sub Total</span>
                                <span class="total_update_price ml-5">
                                    
                                </span>
                            </h3>
                              <h3 class="d-flex justify-content-between">
                                  <span class="text-dark">Tax</span>
                                  <span class="total_update_tax ml-5">
                                    
                                </span>
                              </h3>
                              <hr/>
                              <h3 class="d-flex justify-content-between">
                                  <span class="text-dark">Total</span>
                                  <span class="total_update_total ml-5">
                                    
                                </span>
                              </h3>

                          </div>
                      </div>

                      <?php if(guestCheckout()): ?>
                          <a class="ps-btn ps-btn--fullwidth" href="<?php echo e(route('guest.checkout.index')); ?>">Proceed to checkout</a>
                          <?php else: ?>
                            <a class="ps-btn ps-btn--fullwidth" href="<?php echo e(route('checkout.index')); ?>">Proceed to checkout</a>
                          <?php endif; ?>

                    </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <script>
        "use strict"
        $(document).ready(function (){
        //
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/shopping_cart/guestCart.blade.php ENDPATH**/ ?>