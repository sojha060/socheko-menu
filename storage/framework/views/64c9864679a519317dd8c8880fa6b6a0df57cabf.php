
<?php $__env->startSection('title'); ?>Upload Product <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">Upload Product</h3>

            <div class="float-right">
                <a class="btn btn-primary" href="<?php echo e(route('seller.products')); ?>">
                    All Products
                </a>
            </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-2">
            <form method="post" action="<?php echo e(route('seller.product.store')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Select parent category <small class="text-danger">*</small></label>
                            <select class="form-control select2 parent-cat" name="parent_id" required>
                                <option value=""></option>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?>

                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Select child category <small class="text-danger">*</small></label>
                            <input name="childUrl" class="childUrl" type="hidden"
                                   value="<?php echo e(route('admin.child.index')); ?>" required>
                            <input name="productUrl" class="productUrl" type="hidden"
                                   value="<?php echo e(route('admin.product.index')); ?>" required>
                            <select class="form-control select2 childCatShow" name="category_id" required></select>
                        </div>

                        <div class="form-group">
                            <label>Select Product <small class="text-danger">*</small></label>
                            <input class="productdetailsUrl" type="hidden"
                                   value="<?php echo e(route('admin.product.show')); ?>">
                            <select class="form-control select2 productShow" name="product_id" required></select>
                        </div>

                        <div class="form-group">
                            <label>Price <small class="text-danger">*</small></label>

                            <input name="product_price" min="0" class="form-control" type="number" step="0.01" required
                                   placeholder="Product price">
                        </div>

                        <div class="form-group d-none">
                            <label>Purchase Price <small class="text-danger">*</small></label>
                            <input name="purchase_price" class="form-control" type="number"
                                   placeholder="Purchase product price">
                        </div>

                        <div class="form-group">
                            <label>
                                <input id="is_discount" name="is_discount" type="checkbox">
                                Is discount?
                            </label>
                        </div>


                        <div class="form-group d-none" id="discount_price">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Discounted Price</label>
                                    <input name="discount_price" class="form-control" type="number"
                                           placeholder="Discounted price">
                                </div>
                                <input type="hidden" name="discount_type" value="flat">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 px-2">
                        <div class="card card-primary" id="productDetails">
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            <!-- Content starts here:END -->
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/products/product_upload.blade.php ENDPATH**/ ?>