
<?php $__env->startSection('title'); ?>
<?php $__env->stopSection(); ?>
<title><?php echo e(getSystemSetting('type_name')); ?> | System Settings</title>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="card m-2">
                <div class="card-header">
                    <h2 class="card-title">System Setting</h2>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo e(route('business.setting.store')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <!--seller-->
                        <label class="label d-none">Seller status</label>
                        <input type="hidden" value="seller" name="seller">
                        <select name="seller_status" class="form-control d-none">
                            <option value="enable" <?php echo e(getSystemSetting('seller') == 'enable' ? 'selected':null); ?>>
                                Enable
                            </option>
                            <option value="disable" <?php echo e(getSystemSetting('seller') == 'disable' ? 'selected':null); ?>>
                                Disable
                            </option>
                        </select>


                        <?php if(sellerStatus()): ?>
                            <label class="label">Publish Mode</label>
                            <input type="hidden" value="seller_mode" name="seller_mode">
                            <select name="mode_status" class="form-control select2">
                                <option
                                    value="request" <?php echo e(getSystemSetting('seller_mode') == 'request' ? 'selected':null); ?>>
                                    Seller can request
                                </option>
                                <option
                                    value="freedom" <?php echo e(getSystemSetting('seller_mode') == 'freedom' ? 'selected':null); ?>>
                                    Seller can publish
                                </option>
                            </select>
                        <?php endif; ?>


                    <!--Primary Color Picker -->
                        <div class="form-group mt-2 d-none">
                            <label>Primary Color</label>
                            <input type="hidden" value="primary_color" name="primary_color">
                            <div class="input-group my-colorpicker2">
                                <input type="text" class="form-control" name="p_color"
                                       value="<?php echo e(getSystemSetting('primary_color')); ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-square"
                                                                      style="color: <?php echo e(getSystemSetting('primary_color')); ?>"></i></span>
                                </div>
                            </div>
                        </div>

                        
                        <div class="form-group d-none">
                            <label>Secondary Color</label>
                            <input type="hidden" value="secondary_color" name="secondary_color">
                            <div class="input-group my-colorpicker3">
                                <input type="text" class="form-control" name="s_color"
                                       value="<?php echo e(getSystemSetting('secondary_color')); ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-square"
                                                                      style="color: <?php echo e(getSystemSetting('secondary_color')); ?>"></i></span>
                                </div>
                            </div>
                        </div>

                        <!--verifications--->
                        <label class="label mt-2">Email Verifications</label>
                        <input type="hidden" value="verification" name="verification">
                        <select name="verification_status" class="form-control select2">
                            <option value="on" <?php echo e(getSystemSetting('verification') == 'on' ? 'selected':null); ?>>
                                On
                            </option>
                            <option value="off" <?php echo e(getSystemSetting('verification') == 'off' ? 'selected':null); ?>>
                                Off
                            </option>
                        </select>

                        <!--Guest checkout--->
                        <label class="label mt-2">Allow Guest Checkout?</label>
                        <input type="hidden" value="checkout" name="checkout">
                        <select name="guest_status" class="form-control select2">
                            <option value="YES" <?php echo e(env('GUEST_CHECKOUT') == 'YES' ? 'selected':null); ?>>
                                Active
                            </option>
                            <option value="NO" <?php echo e(env('GUEST_CHECKOUT') == 'NO' ? 'selected':null); ?>>
                                Block
                            </option>
                        </select>

                        <!--verifications--->
                        <label class="label d-none">Customer Login Mode</label>
                        <input type="hidden" value="login_modal" name="login_modal">
                        <div class="d-none">
                            <select name="login_status" class="form-control select2  d-none">
                                <option value="on" <?php echo e(getSystemSetting('login_modal') == 'on' ? 'selected':null); ?>>
                                    On Modal
                                </option>
                                <option value="off" <?php echo e(getSystemSetting('login_modal') == 'off' ? 'selected':null); ?>>
                                    Off Modal
                                </option>
                            </select>
                        </div>

                        <div class="m-2 float-right">
                            <button class="btn btn-block btn-primary" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>



<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/settings/business.blade.php ENDPATH**/ ?>