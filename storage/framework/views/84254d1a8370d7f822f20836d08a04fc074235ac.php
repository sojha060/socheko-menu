
<?php $__env->startSection('title'); ?> Parent Categories
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'All Category'); ?>
<?php $__env->startSection('content'); ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title"><span><a class="btn btn-sm btn-secondary" href="<?php echo e(route('categories.index')); ?>"><i class="fa fa-reply"></i> </a></span> Category Group: <span class=" font-weight-bold"><?php echo e($cat->name); ?></span></h2>
            </div>
            <div class="float-right">
                <div class="row text-right">
                    <div class="col">
                        <a href="#!"
                           onclick="forModal('<?php echo e(route("parent.categories.create",[$cat->id,$cat->slug])); ?>', 'Add parent category');"
                           class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            Add parent category
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-striped table-bordered table-hover text-center">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th class="text-left">Category</th>
                    <th class="text-left">Sub category</th>
                    <th>Icon class</th>
                    <th>Image</th>
                    <th>Publish</th>
                    <th>View</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e($loop->index+1); ?></td>
                        <td class="text-left"><?php echo e($item->name); ?></td>
                        <td  class="text-left">
                            <?php $__empty_2 = true; $__currentLoopData = $item->childrenCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                <span class="badge badge-primary"><a class="text-white" href="<?php echo e(route('child.categories.index',[$item->id,$item->slug])); ?>"><?php echo e($cat->name); ?></a></span><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                N/A
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <?php if($item->icon != null): ?>
                                <i class="<?php echo e($item->icon); ?>"></i>
                            <?php endif; ?>
                        </td>

                        <td>
                            <?php if($item->image != null): ?>
                                <img src="<?php echo e(filePath($item->image)); ?>" width="80" height="80"
                                     class="img-thumbnail" alt="<?php echo e($item->name); ?>">
                            <?php endif; ?>
                        </td>

                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($item->id); ?>"
                                           <?php echo e($item->is_published == true ? 'checked' : null); ?>  data-url="<?php echo e(route('categories.published')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="is_published_<?php echo e($item->id); ?>">
                                    <label class="custom-control-label" for="is_published_<?php echo e($item->id); ?>"></label>
                                </div>
                            </div>
                        </td>


                        <td class="text-center">
                            <a class="btn btn-sm btn-warning w-75"  href="<?php echo e(route('child.categories.index',[$item->id,$item->slug])); ?>">Sub-categories</a>
                        </td>

                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle btn-sm"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a  href="#!" class="nav-link text-black" onclick="forModal('<?php echo e(route('parent.categories.edit', [$item->id,$item->parent_category_id])); ?>', '<?php echo e($item->name); ?>');">Edit</a></li>

                                    <li class="divider"></li>
                                    <li><a class="nav-link text-black" href="#!"
                                           onclick="confirm_modal('<?php echo e(route('categories.destroy', $item->id)); ?>')">Delete</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="9"><h3 class="text-center">No Data Found</h3></td>
                    </tr>
                <?php endif; ?>
                </tbody>

            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/category/parentIndex.blade.php ENDPATH**/ ?>