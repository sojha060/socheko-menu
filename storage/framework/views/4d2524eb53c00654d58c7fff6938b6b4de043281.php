
<?php $__env->startSection('title'); ?>
<?php $__env->stopSection(); ?>
<title><?php echo e(getSystemSetting('type_name')); ?> | Seller payment account</title>
<?php $__env->startSection('parentPageTitle', 'Account Setup'); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-10 offset-1">
            <div class="mx-2 mb-2">
                <div class="card-header">
                    <h2 class="card-title">Payment Account Setup</h2>
                </div>
                <div class="">
                    <form method="post" action="<?php echo e(route('account.update')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php if($account != null): ?>
                            <input type="hidden" name="id" value="<?php echo e($account->id); ?>">
                        <?php endif; ?>

                        <div class="card m-3">
                            <div class="card-header">
                                <p class="font-weight-bold">Bank Setup</p>
                            </div>
                            <div class="card-body">
                                <div class="form-group m-2">
                                    <label class="label">Bank Name</label>
                                    <input type="text" placeholder="Enter Bank Name"
                                           value="<?php echo e($account->bank_name ?? ''); ?>" name="bank_name"
                                           class="form-control">
                                </div>

                                <div class="form-group m-2">
                                    <label class="label">Account Name</label>
                                    <input type="text" placeholder="Enter Account Name"
                                           value="<?php echo e($account->account_name ?? ''); ?>" name="account_name"
                                           class="form-control">
                                </div>

                                <div class="form-group m-2">
                                    <label class="label">Account Number</label>
                                    <input type="text" placeholder="Enter Account Number"
                                           value="<?php echo e($account->account_number ?? ''); ?>" name="account_number"
                                           class="form-control">
                                </div>
                                <div class="form-group m-2">
                                    <label class="label">Routing Number</label>
                                    <input type="number" placeholder="Enter Routing Number"
                                           value="<?php echo e($account->routing_number ?? ''); ?>" name="routing_number"
                                           class="form-control">
                                </div>


                            </div>
                        </div>


                        

                        

                        <div class="float-right">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>





<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/payment/account_create.blade.php ENDPATH**/ ?>