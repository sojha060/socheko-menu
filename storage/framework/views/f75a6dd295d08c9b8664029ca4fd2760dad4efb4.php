
<?php $__env->startSection('title'); ?><?php $__env->stopSection(); ?>
<title><?php echo e(getSystemSetting('type_name')); ?> | Payment Methods</title>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-8 offset-2">
        <form method="post" action="<?php echo e(route('payment.method.store')); ?>">
        <?php echo csrf_field(); ?>
            <div class="card mt-2">
                <div class="card-header">
                    <h2 class="card-title"><i class="fa fa-money text-warning mr-2"></i>Setup Payment Methods</h2>
                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">
                    <h2 class="card-title"></h2> <br/>
                    <div class="fs-12 text-warning">No Payment Gateway implemented right now</div>
                </div>
                
            </div>


   
            
            

            <hr>
            
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/common/setting/payment/payment-methods.blade.php ENDPATH**/ ?>