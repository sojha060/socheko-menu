<script async src="<?php echo e(asset('frontend/plugins/jquery.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/popper.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/owl-carousel/owl.carousel.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/bootstrap4/js/bootstrap.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/imagesloaded.pkgd.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/masonry.pkgd.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/isotope.pkgd.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/jquery.matchHeight-min.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/slick/slick/slick.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/jquery-bar-rating/dist/jquery.barrating.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/slick-animation.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/lightGallery-master/dist/js/lightgallery-all.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/jquery-ui/jquery-ui.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/sticky-sidebar/dist/sticky-sidebar.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/jquery.slimscroll.js')); ?>"></script>
<script async src="<?php echo e(asset('frontend/plugins/select2/dist/js/select2.full.js')); ?>"></script>
<script async src="<?php echo e(asset('js/parsley.js')); ?>"></script>
<script async src="<?php echo e(asset('js/jquery.cookie.js')); ?>"></script>
<!-- custom scripts-->
<script async src="<?php echo e(asset('frontend/js/main.js')); ?>"></script>
<script async src="<?php echo e(asset('js/toastr.js')); ?>"></script>


<script src="<?php echo e(asset('frontend/js/sub-menu.js')); ?>"></script>

<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/assets/js.blade.php ENDPATH**/ ?>