
<?php $__env->startSection('title'); ?> Withdraw
<?php $__env->stopSection(); ?>
<?php $__env->startSection('parentPageTitle', 'Withdraw'); ?>
<?php $__env->startSection('content'); ?>

    <div class="card card-blue card-outline p-3">
        <div class="card-header bg-white">
            <div class="float-left">
                <h2 class="card-title">Withdraw History</h2>
            </div>
            <div class="float-right">

                <div class="row">
                    <div class="col">
                        <a href="#!"
                           onclick="forModal('<?php echo e(route('payments.create')); ?>','Withdraw Request')"
                           class="btn btn-primary">
                            <i class="la la-plus"></i>
                            Withdraw Request
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-body bg-white table-responsive p-0">
            <table class="table table-bordered table-hover text-center">
                <thead>
                <tr>
                    <th>S/L</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th>Requested On</th>
                    <th>Info</th>
                    <th>Status/Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e(($loop->index+1) + ($payments->currentPage() - 1)*$payments->perPage()); ?></td>
                        <td><?php echo e(formatPrice($item->amount)); ?> </td>

                        <td>
                            <?php echo Purify::clean($item->description); ?>

                        </td>
                        <td>
                            <?php echo e(date('d-M-y',strtotime($item->created_at)) ?? 'N/A'); ?>

                        </td>
                        <td>
                            <?php echo e($item->status); ?><br>
                            Process : <?php echo e($item->process); ?><br>
                            <?php echo e(date('d-M-y',strtotime($item->status_change_date)) ?? 'N/A'); ?>

                        </td>

                        <?php if($item->status != 'Confirm'): ?>
                            <td>
                                <a class="btn btn-warning"
                                   onclick="confirm_modal('<?php echo e(route('payments.destroy', $item->id)); ?>')"
                                   href="#!">
                                    <i class="fa fa-trash mr-2"></i>Delete</a>
                            </td>
                        <?php else: ?>
                            <td title="payment done"><p class="text-success"><i
                                        class="fa fa-1x  fa-check-circle"></i></p></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                    <tr>
                        <td colspan="8"><h3 class="text-center">No Data Found</h3></td>
                    </tr>

                <?php endif; ?>
                </tbody>
                <div class="float-left">
                    <?php echo e($payments->links()); ?>

                </div>
            </table>
        </div>
    </div>



<?php $__env->stopSection(); ?>





<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/payment/payment_index.blade.php ENDPATH**/ ?>