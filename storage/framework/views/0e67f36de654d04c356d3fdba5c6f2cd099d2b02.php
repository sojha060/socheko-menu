<div class="card-body">
    <form action="<?php echo e(route('parent.categories.store')); ?>" method="post" enctype="multipart/form-data">
        <input name="parent_id" type="hidden" value="<?php echo e($cat->id); ?>">
        <input name="parent_slug" type="hidden" value="<?php echo e($cat->slug); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <label>Name <span class="text-danger">*</span></label>
            <input class="form-control" name="name" placeholder="Name" required>
        </div>

        <div class="form-group">
            <label class="col-form-label text-md-right">Icon Class</label>
            <div class="custom-file">
                <input class="form-control" placeholder="fa fa-address-book-o"  name="icon" type="text">
                <small>Want more icon ? <a href="https://fontawesome.com/v4.7.0/icons/"  target="_blank">Font Awesome</a></small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-form-label text-md-right">Image</label>
            <div class="">
                <input class="form-control-file sr-file" placeholder="Choose Image  only"   name="image" type="file">
                <small class="text-info">Upload file support png, jpg, svg format</small>
            </div>
        </div>

        <div class="form-group">
            <label>Meta Title</label>
            <input class="form-control" name="meta_title" type="text" max="100" placeholder="Meta title">
            <small class="text-info">Google standard 100 characters</small>
        </div>

        <div class="form-group">
            <label>Meta Description</label>
            <input class="form-control form-control-lg" name="meta_desc" max="200" placeholder="Meta description">
            <small class="text-info">Google standard 200 characters</small>
        </div>

        <div class="float-right">
            <button class="btn btn-primary float-right" type="submit">Save</button>
        </div>

    </form>
</div>




<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/products/category/parentCategoryCreate.blade.php ENDPATH**/ ?>