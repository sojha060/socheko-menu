<div class="ps-panel--sidebar" id="cart-mobile">
    <div class="ps-panel__header">
        <h3>Shopping Cart</h3>
    </div>
    <div class="navigation__content">
        <div class="ps-cart--mobile">
            <div class="ps-cart__content">
                <div class="ps-product--cart-mobile">


                    

                    <input type="hidden" id="check-out-url" value="<?php echo e(route('checkout.index')); ?>">

                    <input type="hidden" id="shopping-cart-url" value="<?php echo e(route('shopping.cart')); ?>">


                    
                    
                        <input type="hidden" id="guest-check-out-url" value="<?php echo e(route('guest.checkout.index')); ?>">


                        <input type="hidden" id="guest-shopping-cart-url" value="<?php echo e(route('guest.shopping.cart')); ?>">

                        

                    

                    <span class="show-cart-items">
                          
                    </span>
                </div>
            </div>
            <div class="ps-cart__footer cart-items-footer">
                
            </div>
        </div>
    </div>
</div>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/include/cart/shopping-cart.blade.php ENDPATH**/ ?>