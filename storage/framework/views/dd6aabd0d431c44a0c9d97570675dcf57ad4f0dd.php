



<?php $__env->startSection('title'); ?>

<?php $__env->startSection('content'); ?>
    <div class="ps-page--simple">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                    <li>Comparison</li>
                </ul>
            </div>
        </div>
        <div class="ps-section--shopping ps-shopping-cart">
            <div class="mx-5 margin-compare">
                <div class="ps-section__header">
                    <h1>Comparison</h1>
                </div>

                <div class="ps-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered ps-table--shopping-cart text-center">
                            <thead></thead>
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td class="text-danger text-center">
                                        <a href="<?php echo e(route('single.product',[$item->sku,$item->slug])); ?>">
                                            <?php echo e($item->name); ?>

                                        </a>
                                    </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td class="text-center">
                                            <img src="<?php echo e(filePath($item->image)); ?>" class="img-fluid table-compare-img rounded-sm"/>
                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                                <tr>
                                    <td>Price Range</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td class="text-center">
                                            <?php echo e($item->range); ?>

                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                                

                                <tr>
                                    <td>Category</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td class="text-center">
                                            <?php echo e($item->category); ?>

                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                                <tr>
                                    <td>Overview</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td class="text-center">
                                           <?php echo Purify::clean($item->short_desc); ?>

                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                                <tr>
                                    <td>Description</td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td>
                                            <?php echo Purify::clean($item->big_desc); ?>

                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                                <tr>
                                    <td></td>
                                    <?php $__currentLoopData = $comparison_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td class="text-center">
                                            <a href="<?php echo e(route('single.product',[$item->sku,$item->slug])); ?>" class="ps-btn">Add to cart</a>
                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/comparison/index.blade.php ENDPATH**/ ?>