

<?php $__env->startSection('title'); ?> Restaurant Registration <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="ps-contact-form">
        <div class="container">


            <form class="ps-form--contact-us needs-validation" novalidate action="<?php echo e(route('vendor.store')); ?>"
                  method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <h3>Register Your Restaurant</h3>
                <?php if($errors->any()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong><i class="icon-remove-sign"></i> <?php echo e($error); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <div class="row">


                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <h5>PERSONAL INFORMATION</h5>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">

                        <div class="form-group">
                            <input class="form-control" name="name" required value="<?php echo e(old('name')); ?>" type="text"
                                   placeholder="Name *">

                            <div class="invalid-feedback">
                                Name is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="email" value="<?php echo e(old('email')); ?>" type="text" required
                                   placeholder="Email *">

                            <div class="invalid-feedback">
                                Email is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>

                        </div>
                    </div>


                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <h5>RESTAURANT INFORMATION</h5>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="shop_name" value="<?php echo e(old('shop_name')); ?>" type="text"
                                   required placeholder="Restaurant Name *">

                            <div class="invalid-feedback">
                                Restaurant name is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="type" value="<?php echo e(old('type')); ?>"
                                   type="text" required placeholder="Restaurant Type ">

                            <div class="invalid-feedback">
                                Restaurant type is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="trade_licence" value="<?php echo e(old('trade_licence')); ?>"
                                   type="text" required placeholder="Trade Licence *">

                            <div class="invalid-feedback">
                                Trade licence is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="phone" value="<?php echo e(old('phone')); ?>" type="number" required
                                   placeholder="Restaurant Phone Number *">

                            <div class="invalid-feedback">
                                Restaurant phone number is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" name="address" type="text" value="<?php echo e(old('address')); ?>" required
                                   placeholder="Restaurant Address *">

                            <div class="invalid-feedback">
                                Restaurant address is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>


                    
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="pt-3 form-control" required name="shop_logo" value="<?php echo e(old('shop_logo')); ?>"
                                   type="file">
                            <small>Restaurnat Logo *</small>

                            <div class="invalid-feedback">
                                Restaurant Logo is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>

                    </div>

                    
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="pt-3 form-control" required name="feature_photo" value="<?php echo e(old('feature_photo')); ?>"
                                   type="file">
                            <small>Cover Photo *</small>

                            <div class="invalid-feedback">
                                Cover Photo is required.
                            </div>

                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>

                    </div>


                </div>
                <div class="form-group submit">
                    <button class="ps-btn" type="submit">Apply</button>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/seller/register.blade.php ENDPATH**/ ?>