
<?php $__env->startSection('title'); ?><?php $__env->stopSection(); ?>
<title> <?php echo e(getSystemSetting('type_name')); ?> | Switch Application Mode </title>
<?php $__env->startSection('content'); ?>

    <div class="col-md-6 offset-3 mt-5">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Switch Application Mode</h3>
            </div>

            <div class="card-body mt-3 ">
                <form action="<?php echo e(route('smtp.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="text-center">
                        <p class="text-white text-uppercase"><span class="bg-primary rounded-sm p-2">Activated Mode : <?php echo e(env('APP_ACTIVE')); ?></span></p>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="types[]" value="APP_ACTIVE">
                        <div class="">
                            <label class="control-label">Active <span
                                        class="text-danger">*</span></label>
                        </div>
                        <div class="">
                            <select class="form-control select2" name="APP_ACTIVE">
                                <option value="ecommerce" <?php if(env('APP_ACTIVE') == "ecommerce"): ?> selected <?php endif; ?>> Ecommerce </option>
                                <option value="vendor" <?php if(env('APP_ACTIVE') == "vendor"): ?> selected <?php endif; ?>>Vendor </option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-primary">Active</button>
                </form>
            </div>

        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/settings/switch_ecommerce.blade.php ENDPATH**/ ?>