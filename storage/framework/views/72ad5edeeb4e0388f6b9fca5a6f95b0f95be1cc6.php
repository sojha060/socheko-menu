

<?php $__env->startSection('title'); ?> Order <?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="ps-page--single">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                    <li><a href="<?php echo e(route('customer.orders')); ?>">Your order</a></li>
                    <li>#<?php echo e($order_detail->order_number); ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ps-vendor-dashboard pro">
        <div class="container">
            <div class="ps-section__header">
                <h3>Customer Dashboard</h3>
            </div>
            <div class="ps-section__content">
                <ul class="ps-section__links">
                    <li class="active"><a href="<?php echo e(route('customer.orders')); ?>">Your Order</a></li>
                    <li><a href="<?php echo e(route('customer.index')); ?>">Your Profile</a></li>
                    <?php if(affiliateRoute() && affiliateActive()): ?>
                    <li><a href="<?php echo e(route('customers.affiliate.registration')); ?>">Affiliate Marketing</a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo e(route('logout')); ?>"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Sign Out
                        </a>
                        <form id="logout-form" class="d-none" action="<?php echo e(route('logout')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                        </form>
                    </li>
                </ul>
            </div>


            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">


                    <figure class="ps-block--vendor-status">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                <figcaption>Order #<?php echo e($order_detail->order_number); ?></figcaption>
                                <p class="text-uppercase h3">
                                    Total: <?php echo e(formatPrice($order_detail->pay_amount)); ?></p>
                                <p class="text-uppercase h4">Payment
                                    Type: <?php echo e($order_detail->payment_type); ?></p>
                                <p class="h4">Order date
                                    : <?php echo e($order_detail->created_at->format('M d, Y')); ?></p>
                            </div>
                            <div class="col-md-6">
                                <figcaption>Delivery Information</figcaption>
                                <p class="h3">Address: <?php echo e($order_detail->address); ?>

                                </p>

                                <p class="h3">Shipping Via: <?php echo e($order_detail->logistic->name); ?>

                                </p>

                                <p class="h3">Shipping
                                    Rate: <?php echo e(formatPrice($order_detail->logistic_charge)); ?>

                                </p>

                                <p class="h3">Shipping Zone:
                                    Division: <?php echo e($order_detail->division->district_name); ?>

                                </p>
                                <p class="h3">
                                    Area: <?php echo e($order_detail->area->thana_name); ?>

                                </p>
                            </div>
                        </div>


                        <?php $__currentLoopData = $order_detail->order_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="_2oLsaWi9Nj">
                                <div class="_3wII7s9lsM">

                                    <img src="<?php echo e(filePath( $product->product->product->image )); ?>" sizes="4vw"
                                         width="800" class="w-20">

                                    <div class="_3zeUfCZnvZ">
                                        <span
                                            class="_3IytpSfnoZ fs-30"><?php echo e($product->product->product->name); ?>  <?php echo e(\Illuminate\Support\Str::upper($product->vendor_product_stock->product_variants)); ?></span>
                                        <p class="">Sold by - <?php echo e($product->shop->shop_name ?? ''); ?></p>
                                        <p class="">Booking Code - #<?php echo e($product->booking_code); ?></p>
                                        <p class="">Status -

                                            <?php echo e($product->status === 'pending' ? 'pending' : ''); ?>

                                            <?php echo e($product->status === 'canceled' ? 'cancel' : ''); ?>

                                            <?php echo e($product->status === 'delivered' ? 'delivered' : ''); ?>

                                            <?php echo e($product->status === 'follow_up' ? 'follow up' : ''); ?>


                                        </p>
                                        <a href="<?php echo e(route('customer.tracking.order.number', $product->booking_code)); ?>"
                                           class="badge badge-success">Track Order</a>

                                        <?php
                                            $check_complain = App\Models\Complain::where('booking_code', $product->booking_code)->exists();
                                        ?>

                                        <?php if($product->status == 'delivered'): ?>
                                            <?php if(!$check_complain): ?>
                                                <a href="javascript:void(0)"
                                                   onclick="forModal('<?php echo e(route('customer.complain.index', $product->booking_code)); ?>', 'Product Complain')"
                                                   class="badge badge-danger">Make Complain</a>
                                            <?php else: ?>
                                                <a href="javascript:void(0)"
                                                   onclick="forModal('<?php echo e(route('customer.complain.review', $product->booking_code)); ?>',' Product Complain')"
                                                   class="badge badge-danger">View Complain</a>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if($product->status == 'delivered'): ?>
                                            <?php if(empty($product->review)): ?>
                                                <a href="javascript:void(0)"
                                                   onclick="forModal('<?php echo e(route('customer.product.review', $product->booking_code)); ?>', 'Product Review')"
                                                   class="badge badge-primary">Give Review</a>
                                            <?php else: ?>
                                                <a href="javascript:void(0)"
                                                   onclick="forModal('<?php echo e(route('customer.product.review', $product->booking_code)); ?>', 'Product Review')"
                                                   class="badge badge-primary">Your Review</a>
                                            <?php endif; ?>
                                        <?php endif; ?>


                                    </div>
                                </div>
                                <div class="_3oGH8BD26C">
                                    <div class="_2qea2ZM2jo">
                                        <span class="_3hUqCdCiBb fs-22">Qty :</span>
                                        <span class="_3hQCdklHSO fs-22"><?php echo e($product->quantity); ?></span>
                                    </div>
                                    <div class="_1SLnXV0Syg fs-22"><?php echo e(formatPrice($product->product_price)); ?></div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </figure>
                </div>


            </div>


        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/order/order_details.blade.php ENDPATH**/ ?>