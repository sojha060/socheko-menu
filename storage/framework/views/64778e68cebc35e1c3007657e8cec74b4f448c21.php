<form action="<?php echo e(route('currencies.update')); ?>" method="post" enctype="multipart/form-data">
    <?php echo csrf_field(); ?>
    <input type="hidden" name="id" value="<?php echo e($currency->id); ?>">
    <div class="form-group">
        <label for="name" class="col-form-label text-md-right">Name <span
                class="text-danger">*</span></label>
        <input placeholder="Ex : United State" type="text" class="form-control" value="<?php echo e($currency->name); ?>" name="name"
               autofocus required>
    </div>

    <div class="form-group">
        <label for="name" class="col-form-label text-md-right">Symbol <span
                class="text-danger">*</span></label>
        <input placeholder="Ex : $" type="text" class="form-control" value="<?php echo e($currency->symbol); ?>" name="symbol"
               required>
    </div>

    <div class="form-group">
        <label for="name" class="col-form-label text-md-right">Code <span
                class="text-danger">*</span></label>
        <input placeholder="Ex : USD" type="text" class="form-control" value="<?php echo e($currency->code); ?>" name="code" required>
    </div>

    <div class="form-group">
        <label for="name" class="col-form-label text-md-right">Exchange Rate Ex: 1 USD = ? <span
                class="text-danger">*</span></label>
        <input min="0.01" step="0.01" placeholder="Ex: 1 USD = ?" type="number" class="form-control" name="rate"
               required
               value="<?php echo e($currency->rate); ?>">
    </div>

    <div class="form-group">
        <label class="control-label">Select country flag <span
                class="text-danger">*</span></label>
        <div class="">
            <select class="form-control lang" name="image" required>
                <option value=""></option>
                <?php $__currentLoopData = readFlag(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($loop->index >1): ?>
                        <option value="<?php echo e($item); ?>"
                                data-image="<?php echo e(asset('images/lang/'.$item)); ?>" <?php echo e($item == $currency->image ? 'selected':null); ?>> <?php echo e(flagRenameAuto($item)); ?></option>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
        </div>
    </div>

    <div class="float-right">
        <button class="btn btn-primary" type="submit">Save</button>
    </div>

</form>
<?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/common/setting/currency/edit.blade.php ENDPATH**/ ?>