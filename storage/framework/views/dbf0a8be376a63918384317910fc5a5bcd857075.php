


<?php $__env->startSection('title'); ?> Restaurant <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- breadcrumb:START -->
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo e(route('homepage')); ?>">Home</a></li>
                <li>Restaurant</li>
            </ul>
        </div>
    </div>
    <!-- breadcrumb:END -->

    <!-- SHOP LIST:START -->
    <div class="ps-section--shopping ps-whishlist">
        <div class="container">
            <div class="ps-section__header">
                <h1>All Restaurant</h1>
            </div>
            <div class="ps-section__content">
                <!-- all brand list -->
                <div class="container">
                    <form class="ps-form--newsletter" action="<?php echo e(route('search.vendor')); ?>" method="get">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div class="ps-form__right">
                                   
                                    <div class="form-group--nest">
                                        <input class="form-control" type="text" id="myInput" name="q" value="<?php echo e(@$q); ?>"
                                               placeholder="Search restaurant here">
                                        <button class="ps-btn">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="container" id="myShop">
                    <div class="row t-mt-30">
                        <!-- HERE GOES SHOPS -->
                        <?php $__empty_1 = true; $__currentLoopData = $seller_shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $seller_shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <div class="col-md-3 col-xl-3 t-mb-30">
                                <a href="<?php echo e(route('vendor.shop',$seller_shop->vendor->slug)); ?>" class="product-card">

                                    <?php if(empty($seller_shop->vendor->shop_logo)): ?>
                                        <span class="product-card__img-wrapper">
                                            <img src="<?php echo e(asset('vendor-store.jpg')); ?>"
                                             alt="<?php echo e($seller_shop->vendor->shop_name); ?>" class="img" height="100px">
                                        </span>
                                    <?php else: ?>

                                    <span class="product-card__img-wrapper">
                                            <img src="<?php echo e(filePath($seller_shop->vendor->shop_logo)); ?>"
                                            
                                            alt="<?php echo e($seller_shop->vendor->shop_name); ?>" class="img" height="100px">
                                        </span>

                                    <?php endif; ?>


                                    <span class="product-card__body">
                                        <span class="product-card__title text-center h3 text-capitalize font-weight-bold">
                                            <?php echo e($seller_shop->vendor->shop_name); ?>

                                        </span>
                                    </span>
                                </a>
                            </div>
                            
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <div class="col-md-12">
<img src="<?php echo e(asset('shop-not-found.png')); ?>" class="img-fluid" alt="#shop-not-found">
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- SHOP LIST:END -->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/frontend/seller/seller_shops.blade.php ENDPATH**/ ?>