
<?php $__env->startSection('title'); ?> <?php echo e($seller_profile->shop_name); ?>  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="card m-2">
                <div class="card-header">
                    <h2 class="card-title"><?php echo e($seller_profile->shop_name); ?> Profile</h2>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo e(route('vendor.update')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>

                    <!--logo-->
                        <label class="label">Restaurant Logo</label>

                        <div class="avatar-upload">

                            <div class="avatar-edit">
                                <input type='file' name="shop_logo" id="imageUpload" accept=".png, .jpg, .jpeg"/>
                                <label for="imageUpload"></label>
                            </div>

                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url(<?php echo e(filePath($seller_profile->vendor->shop_logo)); ?>);">
                                </div>
                            </div>



                        </div>

                        <div class="card-body">

                                <div class="form-group row">
                                    <label for="owner" class="col-sm-2 col-form-label">Shop owner</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo e($seller_profile->vendor->shop_name); ?>" id="owner" placeholder="Shop owner" name="shop_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Shop email</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="email" placeholder="email" value="<?php echo e($seller_profile->vendor->email); ?>" name="email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="number" class="col-sm-2 col-form-label">Shop number</label>
                                    <div class="col-sm-10">
                                    <input type="number" class="form-control" value="<?php echo e($seller_profile->vendor->phone); ?>" id="number" placeholder="Number" name="phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nationality" class="col-sm-2 col-form-label">Nationality</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo e($seller_profile->nationality); ?>" id="nationality" placeholder="Nationality" name="nationality">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Gender</label>
                                    <label for="male"><input type="radio" id="male" name="genders" value="Male" <?php if($seller_profile->genders === 'Male'): ?> return checked <?php endif; ?>> Male</label>
                                    <label for="female"><input type="radio" id="female" name="genders" value="Female" <?php if($seller_profile->genders === 'Female'): ?> return checked <?php endif; ?>>  Female</label>
                                    <label for="other"><input type="radio" id="Other" name="genders" value="Other" <?php if($seller_profile->genders === 'Other'): ?> return checked <?php endif; ?>>  Other</label>

                                </div>

                                <div class="form-group row">
                                    <label for="trade_licence" class="col-sm-2 col-form-label">Trade Licence</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="trade_licence" value="<?php echo e($seller_profile->vendor->trade_licence); ?>" name="trade_licence" placeholder="Trade Licence" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="address" class="form-control" id="address" value="<?php echo e($seller_profile->vendor->address); ?>" placeholder="Address">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="about" class="col-sm-2 col-form-label">About</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="about" class="form-control" id="about" value="<?php echo e($seller_profile->vendor->about); ?>" placeholder="About Your Shop">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="facebook" class="col-sm-2 col-form-label">Facebook</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="facebook" class="form-control" id="facebook" value="<?php echo e($seller_profile->vendor->facebook); ?>" placeholder="Facebook Link">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control" id="inputPassword3" value="<?php echo e($seller_profile->password); ?>" placeholder="Password">
                                    </div>
                                </div>


                                


                                <div class="form-group row">

                                 

                                    <label for="inputPassword3" class="col-sm-2 col-form-label">Feature Photo</label>

                                  
                                    <div class="col-sm-10">
                                    <input type="file" name="feature_photo" >
                                    </div>

                                    <div class="col-sm-2">
                                        <?php if($seller_profile): ?>
                                
                                        <img height="100px" src="<?php echo e(filePath($seller_profile->vendor->feature_photo)); ?>" alt="">

                                 
                                    <?php endif; ?> 
                                    </div>
                                </div>

                                <div class="form-group row">
                                   <button class="btn btn-primary" type="submit">Save changes</button>
                                </div>

                                </div>
                        <!--logo end-->

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>



<?php $__env->startSection('js-link'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/sellers/profile/profile.blade.php ENDPATH**/ ?>