
<?php $__env->startSection('title'); ?> Currency <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="card m-2">
                <div class="card-header">
                    <h2 class="card-title">Setup Currency Setting</h2>
                </div>
                <div class="card-body">
                    <form method="post" action="<?php echo e(route('currencies.default')); ?>">
                        <?php echo csrf_field(); ?>

                        <label class="label">Select Default</label>
                        <input type="hidden" value="default_currencies" name="type_default">
                        <select class="form-control select2" name="default">
                            <option value=""></option>
                            <?php $__currentLoopData = $dCurrencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option
                                    value="<?php echo e($item->id); ?>" <?php echo e(getSystemSetting('default_currencies') == $item->id ? 'selected' : null); ?>><?php echo e($item->symbol); ?> <?php echo e($item->code); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <div class="m-2 text-center">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">Currency List</h2>
            </div>
            <div class="float-right">
                <a href="javascript:void()"
                   onclick="forModal('<?php echo e(route("currencies.create")); ?>','Create Currency')"
                   class="btn btn-primary">
                    <i class="la la-plus"></i>
                    Create A Currency
                </a>
            </div>
        </div>
        <div class="card-body">
            <!-- there are the main content-->
            <table class="table table-striped- table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Symbol</th>
                    <th>Code</th>
                    <th>Flag</th>
                    <th>Rate</th>
                    <th>Align</th>
                    <th>Publish</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>

                        <td><?php echo e($loop->index+1); ?></td>
                        <td><?php echo e($item->name); ?></td>
                        <td><?php echo e($item->symbol); ?></td>
                        <td><?php echo e($item->code); ?></td>
                        <td>
                            <img
                                src="<?php echo e(asset('images/lang/'.$item->image)); ?>" class="" height="30px"
                                alt=""/>
                        </td>
                        <td><?php echo e($item->rate); ?></td>
                        <td class="pt-0">


                            <div class="mid pt-0">
                                <label class="rocker rocker-small mt-0">
                                    <input type="checkbox" data-id="<?php echo e($item->id); ?>"
                                           data-url="<?php echo e(route('currencies.align')); ?>" <?php echo e($item->align == true ? 'checked':null); ?>>
                                    <span class="switch-left">Left</span>
                                    <span class="switch-right">Right</span>
                                </label>
                            </div>

                        </td>
                        <td>
                            <div class="form-group">
                                <div
                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="<?php echo e($item->id); ?>"
                                           <?php echo e($item->is_published == true ? 'checked':null); ?>  data-url="<?php echo e(route('currencies.published')); ?>"
                                           type="checkbox" class="custom-control-input"
                                           id="customSwitch<?php echo e($item->id); ?>">
                                    <label class="custom-control-label" for="customSwitch<?php echo e($item->id); ?>"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group-vertical">
                                <?php if($item->id != 1): ?>
                                <a class="btn btn-info"
                                   href="#!"
                                   onclick="confirm_modal('<?php echo e(route('currencies.destroy', $item->id)); ?>')">Delete</a>
                                <?php endif; ?>
                                <a class="btn btn-warning" href="#!"
                                   onclick="forModal('<?php echo e(route('currencies.edit', $item->id)); ?>','Currency Update')">
                                    Edit</a>

                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="6"><h3 class="text-center">No data Found</h3></td>
                    </tr>
                <?php endif; ?>

                </tbody>
            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\socheko-menu\resources\views/backend/common/setting/currency/index.blade.php ENDPATH**/ ?>