-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2020 at 11:19 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multivendor`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_commissions`
--

CREATE TABLE `admin_commissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `confirm_by` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `percentage` double(8,2) NOT NULL,
  `commission` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `is_requested` tinyint(1) NOT NULL DEFAULT 0,
  `meta_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer` bigint(20) UNSIGNED NOT NULL,
  `start_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_at` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_for_seller` tinyint(1) NOT NULL DEFAULT 1,
  `active_for_customer` tinyint(1) NOT NULL DEFAULT 0,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_requested` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_products`
--

CREATE TABLE `campaign_products` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `vpvs_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popular` tinyint(1) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL DEFAULT 0,
  `is_published` tinyint(1) NOT NULL DEFAULT 0,
  `is_requested` tinyint(1) NOT NULL DEFAULT 0,
  `parent_category_id` int(11) NOT NULL DEFAULT 0,
  `cat_group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `meta_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_percentage` double DEFAULT NULL,
  `end_percentage` double DEFAULT NULL,
  `start_amount` double DEFAULT NULL,
  `end_amount` double DEFAULT NULL,
  `commission_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commissions`
--

CREATE TABLE `commissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `type` enum('percentage','flat') COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_amount` double DEFAULT NULL,
  `end_amount` double DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `complain_photos` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('solved','Not Solved','Untouched') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double NOT NULL,
  `start_day` datetime NOT NULL,
  `end_day` datetime NOT NULL,
  `min_value` double DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` double NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `align` tinyint(1) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `rate`, `is_published`, `align`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Dollar', 'USD', '$', 1, 1, 1, 'Flag_of_the_United_States.png', NULL, '2020-09-02 03:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar.jpg',
  `phn_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `district_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_campaign_products`
--

CREATE TABLE `ecom_campaign_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_carts`
--

CREATE TABLE `ecom_carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_stock_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_orders`
--

CREATE TABLE `ecom_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `area_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_order_products`
--

CREATE TABLE `ecom_order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `order_number` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_stock_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','delivered','canceled','follow_up','processing','quality_check','product_dispatched','confirmed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_star` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentedBy` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_product_variant_stocks`
--

CREATE TABLE `ecom_product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT 0,
  `alert_quantity` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `slug`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'super-admin', NULL, NULL, '2020-08-12 00:19:29', '2020-09-26 00:37:42'),
(2, 'customer', 'customer', NULL, NULL, '2020-08-12 04:06:36', '2020-08-12 04:06:36'),
(3, 'seller', 'seller', NULL, NULL, '2020-08-12 04:08:56', '2020-08-17 21:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `group_has_permissions`
--

CREATE TABLE `group_has_permissions` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_has_permissions`
--

INSERT INTO `group_has_permissions` (`group_id`, `permission_id`) VALUES
(2, 111),
(3, 106),
(3, 110),
(3, 113),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 107),
(1, 108),
(1, 109),
(1, 112),
(1, 115),
(1, 116),
(1, 106),
(1, 113),
(1, 114);

-- --------------------------------------------------------

--
-- Table structure for table `infopages`
--

CREATE TABLE `infopages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section` enum('top','bottom') COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'Flag_of_the_United_States.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logistics`
--

CREATE TABLE `logistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_areas`
--

CREATE TABLE `logistic_areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logistic_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` bigint(20) UNSIGNED NOT NULL,
  `area_id` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_16_005237_create_permissions_table', 1),
(4, '2019_03_16_005538_create_user_has_permissions_table', 1),
(5, '2019_03_16_005634_create_groups_table', 1),
(6, '2019_03_16_005759_create_group_has_permissions_table', 1),
(7, '2019_03_16_005834_create_user_has_groups_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_05_21_093740_create_settings_table', 1),
(10, '2020_05_21_093839_create_categories_table', 1),
(11, '2020_05_21_093908_create_currencies_table', 1),
(12, '2020_05_21_093935_create_languages_table', 1),
(13, '2020_05_21_203324_create_pages_table', 1),
(14, '2020_05_21_203341_create_page_contents_table', 1),
(15, '2020_06_12_164431_create_modules_table', 1),
(16, '2020_06_12_164655_create_module_has_permissions_table', 1),
(17, '2020_06_17_075327_create_brands_table', 1),
(18, '2020_06_18_152443_create_commissions_table', 1),
(19, '2020_06_18_205802_create_vendors_table', 1),
(20, '2020_06_19_061010_create_category_groups_table', 1),
(21, '2020_06_20_063516_create_section_settings_table', 1),
(22, '2020_06_23_080421_create_variants_table', 1),
(24, '2020_06_24_045956_create_product_images_table', 1),
(25, '2020_06_24_050102_create_product_variants_table', 1),
(26, '2020_06_27_094308_create_customers_table', 1),
(27, '2020_06_27_132906_create_vendor_products_table', 1),
(28, '2020_07_04_082306_create_carts_table', 1),
(29, '2020_07_06_052133_create_promotions_table', 1),
(30, '2020_07_06_130948_create_wishlists_table', 1),
(31, '2020_07_08_054549_create_districts_table', 1),
(32, '2020_07_08_054723_create_thanas_table', 1),
(33, '2020_07_08_071829_create_logistics_table', 1),
(34, '2020_07_08_091228_create_logistic_areas_table', 1),
(35, '2020_07_09_084030_create_vendor_product_variant_stocks_table', 1),
(36, '2020_07_09_100615_create_campaigns_table', 1),
(37, '2020_07_09_114210_create_coupons_table', 1),
(38, '2020_07_13_065921_create_campaign_products_table', 1),
(39, '2020_07_23_120041_create_orders_table', 1),
(41, '2020_07_29_094601_create_complains_table', 1),
(42, '2020_08_07_052102_create_page_groups_table', 2),
(43, '2020_07_23_121543_create_order_products_table', 3),
(45, '2020_08_13_092807_create_infopages_table', 5),
(47, '2020_08_10_104715_create_admin_commissions_table', 6),
(48, '2020_08_18_032558_create_seller_earnings_table', 6),
(49, '2020_08_18_071516_create_seller_accounts_table', 7),
(50, '2020_08_18_072008_create_payments_table', 7),
(51, '2020_06_24_044443_create_products_table', 8),
(52, '2020_08_20_062802_create_product_variant_stocks_table', 9),
(53, '2020_08_20_083105_create_ecom_product_variant_stocks_table', 10),
(54, '2020_08_23_034334_create_ecom_campaign_products_table', 11),
(55, '2020_08_23_052929_create_ecom_carts_table', 11),
(56, '2020_08_24_032931_create_ecom_orders_table', 12),
(57, '2020_08_24_033032_create_ecom_order_products_table', 12),
(58, '2020_09_29_071020_create_addons_table', 13),
(59, '2020_09_29_071051_create_addons_table', 14),
(60, '2020_09_29_071656_create_addons_table', 15),
(61, '2020_09_29_080306_create_addons_table', 16),
(62, '2016_06_01_000001_create_oauth_auth_codes_table', 17),
(63, '2016_06_01_000002_create_oauth_access_tokens_table', 17),
(64, '2016_06_01_000003_create_oauth_refresh_tokens_table', 17),
(65, '2016_06_01_000004_create_oauth_clients_table', 17),
(66, '2016_06_01_000005_create_oauth_personal_access_clients_table', 17);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `created_at`, `updated_at`) VALUES
(6, 'backend Module', '2020-08-12 03:18:02', '2020-08-17 20:59:19'),
(7, 'Seller Panel', '2020-08-12 03:36:16', '2020-08-12 03:36:16'),
(8, 'front End', '2020-08-12 03:36:27', '2020-08-12 03:36:27'),
(9, 'order managment', '2020-08-17 20:59:36', '2020-08-17 20:59:36'),
(10, 'Payment manage', '2020-08-18 03:19:46', '2020-08-18 03:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `module_has_permissions`
--

CREATE TABLE `module_has_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_has_permissions`
--

INSERT INTO `module_has_permissions` (`id`, `permission_id`, `module_id`, `created_at`, `updated_at`) VALUES
(164, 111, 8, '2020-08-12 03:36:27', '2020-08-12 03:36:27'),
(196, 110, 7, '2020-08-17 20:58:49', '2020-08-17 20:58:49'),
(225, 106, 9, '2020-08-17 20:59:36', '2020-08-17 20:59:36'),
(226, 113, 9, '2020-08-17 20:59:37', '2020-08-17 20:59:37'),
(227, 114, 10, '2020-08-18 03:19:46', '2020-08-18 03:19:46'),
(228, 82, 6, '2020-08-25 04:35:21', '2020-08-25 04:35:21'),
(229, 83, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(230, 84, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(231, 85, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(232, 86, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(233, 87, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(234, 88, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(235, 89, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(236, 90, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(237, 91, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(238, 92, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(239, 93, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(240, 94, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(241, 95, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(242, 96, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(243, 97, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(244, 98, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(245, 99, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(246, 100, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(247, 101, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(248, 102, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(249, 103, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(250, 104, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(251, 105, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(252, 107, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(253, 108, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(254, 109, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(255, 112, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(256, 115, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(257, 116, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01d149522bd7bf5bd9adb5e5e2f5896b17c7fb483e317c4c37db7b9725bd14669128b0055709f2bf', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-10 22:36:53', '2020-10-10 22:36:53', '2021-10-11 04:36:53'),
('05a6b34801e9a5db4b2cb1642b3a026278fcfbc6cd466f0084085a548815d1a4b2eeae5eaccb37e4', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-18 21:02:18', '2020-10-18 21:02:18', '2021-10-19 03:02:18'),
('08c860760aea6d6529806d424a5d51934142e68b1a8d434a507d089f4fec6551426be097be0c798a', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-11 00:10:57', '2020-10-11 00:10:57', '2021-10-11 06:10:57'),
('0f6dee381d0e9f60262dc46b8c772c82b93a652a277fee45fe20dcb08ec85daf58c512111eefdb25', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-07 01:28:04', '2020-10-07 01:28:04', '2021-10-07 07:28:04'),
('1c5e84dea76bdc49ae34978290a93cb3a95f1df02dee070b44bbf47f247c8e5ebac73fc6e8db2e19', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-10 22:40:30', '2020-10-10 22:40:30', '2021-10-11 04:40:30'),
('22a326ea7f4a2995087218a04cb42822466e86b96f5e7275c5b59946a1a536ce27fab2b9aa13c091', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-19 00:58:39', '2020-10-19 00:58:39', '2021-10-19 06:58:39'),
('23a65b2a04cbb41d1b3b13c97e05be3435764b494104edb8b49a3451935e08cb4eb15d11fc9133b2', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:34:08', '2020-10-12 23:34:08', '2021-10-13 05:34:08'),
('29aacf13fefaa3c72a9bc086bb4f9264d0b84f5723295baacf8644db567d8f913812dde4af1e193c', 16, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-10 22:58:52', '2020-10-10 22:58:52', '2021-10-11 04:58:52'),
('2a05ecd2821583c2c27c2c512b5a3e48a4953b857248d65c3f3a2b6684eb4ac1a38ca07e6191cfc6', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 21:21:44', '2020-10-12 21:21:44', '2021-10-13 03:21:44'),
('2f7550b9f58ea25285ac41da01f9300375a1e70ac704ddc4c7bc3bb18c3b2dbc3dda1aefbfa36ed2', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:33:43', '2020-10-12 23:33:43', '2021-10-13 05:33:43'),
('31938df2f4c1d1c1f44d1656f9970ac071f510a20c369f6117ee0edaf912c0302bd0764bf8cdebc2', 18, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 04:52:11', '2020-10-12 04:52:11', '2021-10-12 10:52:11'),
('33af8ee16c70c5327a17a11a81e443850bd498e7364247ad48945e149f8d487ac1adba53c6a870c4', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-11 20:53:21', '2020-10-11 20:53:21', '2021-10-12 02:53:21'),
('3defb3962f28549769a7833fe5f1d488b6456538c926b214b69893162a23939193974478b513b30b', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-08 04:30:39', '2020-10-08 04:30:39', '2021-10-08 10:30:39'),
('456f86139a2b20e116dc96b08d119347dff8d3e00f0e5b2655b8d0d45e8bca43ef64ef594daf64ce', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-11 23:26:54', '2020-10-11 23:26:54', '2021-10-12 05:26:54'),
('492c0436e1429bca1f06d76020600334669c16a964d637755495ef76b040d14ba927fe75730931b5', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-12 23:31:42', '2020-10-12 23:31:42', '2021-10-13 05:31:42'),
('4c1b4dd49d4e87804c13540e22b3f9f387cea8a668762a7213c6302acefffd286830dee7fa648e54', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 22:52:25', '2020-10-12 22:52:25', '2021-10-13 04:52:25'),
('4e6882d8966bc2b2bfbd9f98c3b24ffb95b3a74d6e7b85733db5b612da0721fc02ef91cc70441b5f', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-18 22:38:07', '2020-10-18 22:38:07', '2021-10-19 04:38:07'),
('4f52a472c6000349120dc5629dda945c973ae59dbbaf6b9b86f16140f69ef69fcc5dcb84c6c4ee6c', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:30:08', '2020-10-12 23:30:08', '2021-10-13 05:30:08'),
('500636f3f36575abe6065efcf5a4791047898c11af30bd1cb3f4c7d9f142b1ba8562cab4bb5f5de7', 14, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-10 22:56:37', '2020-10-10 22:56:37', '2021-10-11 04:56:37'),
('519a189d8bc683f78e9c254483f0ff5ee9344e8537c9f2e5b16abe1845b46fac9a590569673c0848', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-11 00:01:55', '2020-10-11 00:01:55', '2021-10-11 06:01:55'),
('52d36e62f338feb4bc0a8ca4d20eeb48be08ed98914510bc96c3c2034cd38dc003588f1722c27cb8', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 21:19:57', '2020-10-12 21:19:57', '2021-10-13 03:19:57'),
('531b4c3fd8706ec0e567e017ce1dbebd4c7c84bb94a63a912a5c2c7e15bf104d4f177f279eb4b88d', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-22 20:22:14', '2020-10-22 20:22:14', '2021-10-22 16:22:14'),
('5609b8fb494aefb5e56b6cc87946de1f5d7684dfd41424d614796649284ef18279e1a777aa84fbc1', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-07 01:27:47', '2020-10-07 01:27:47', '2021-10-07 07:27:47'),
('5d139d19127c40e78e120437a624da95537ca4c82f6d4d8f70b8f2187d06828d690af53e84fb762b', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:29:40', '2020-10-12 23:29:40', '2021-10-13 05:29:40'),
('5dfc3eeda3794716704b0a3977f1187fe062c945cc401d8abd203ec1a52f94f6ecb7cd48c1d383b2', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-12 04:50:45', '2020-10-12 04:50:45', '2021-10-12 10:50:45'),
('661f35eb0a9a955c2804631c3fe7a5be604cd135ac56636406c3fcfc5ee6f3b86788baaf32789ada', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:26:09', '2020-10-12 23:26:09', '2021-10-13 05:26:09'),
('674cef6c0d753fafe2eb2488f4ecd986afd42a39a5364c3b4f6bcdf1e4b80ac16c11478ca2aa2c21', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-08 00:00:56', '2020-10-08 00:00:56', '2021-10-08 06:00:56'),
('72882e59025ec4246e6f4a17c77a8bc555458f414b2fb13a3c96b23e22f7ef228e664922c8e31af3', 13, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-10 22:42:35', '2020-10-10 22:42:35', '2021-10-11 04:42:35'),
('72e176d560a9ad2685bd495a18c9152cfb318246de647d8d853ee5a306b06c07afbf604fbc909e67', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-12 22:50:24', '2020-10-12 22:50:24', '2021-10-13 04:50:24'),
('7dc0dd87759aae6f954a1db7f3dd636cb9eb870d665fded91e75821c3e62a90e3e28d79d7a9ca446', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-18 21:01:51', '2020-10-18 21:01:51', '2021-10-19 03:01:51'),
('7e71d366486b9d54e27a892fd7f51c43e6ecf419fd2802b2b84c236be7e92dbc05174cc6fbef2fe1', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-24 08:16:10', '2020-10-24 08:16:10', '2021-10-24 04:16:10'),
('7ff5c6bd65874a7ee3a42ef38e4d2e6dd95165280c8dc9c2384b1e0e8c2acc072c76919e036d54e3', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-12 21:32:30', '2020-10-12 21:32:30', '2021-10-13 03:32:30'),
('83af709731ec667584a7c31552d13ae6d810759f3da8d1ee3a125d616723deea9517b7f58139e10b', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-24 07:16:46', '2020-10-24 07:16:46', '2021-10-24 03:16:46'),
('863ea202ac7960a23925f975181083732918b99b0c1864449af3eee00cf3d16be7ed6161a1dd6439', 17, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-10 22:59:35', '2020-10-10 22:59:35', '2021-10-11 04:59:35'),
('8aa80fd05ac38f4b58c108bb593e0cd1f19b9859ecfa4a1d2cd716015ce9e0c175771d388b361272', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-11 20:41:49', '2020-10-11 20:41:49', '2021-10-12 02:41:49'),
('8e4f8d4e92fa87856c40a63c13eb858baf011013773f638676dbea19b8d4d2f594a3207baa43f1c4', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-07 02:09:04', '2020-10-07 02:09:04', '2021-10-07 08:09:04'),
('97ebd705e5f01a2c9d3b93c453eab733b19a3a390f0ba0c135ff4db39ff508cd8c8ceaf0c7d3191f', 15, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-10 22:57:49', '2020-10-10 22:57:49', '2021-10-11 04:57:49'),
('a38eec74be599a7ab9d520ccecbae6b3800da11578f6d23ed92c422c51a366f4e7fb882517c60718', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-15 04:35:02', '2020-10-15 04:35:02', '2021-10-15 10:35:02'),
('a56994cb50f2e59f4b6e24fd747061e457c5498acf4e0ceef324f5b5e7f56068b6990ba1865ee559', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-12 21:44:01', '2020-10-12 21:44:01', '2021-10-13 03:44:01'),
('a9f00aeba376d661f969552bbc6adadef9516567064f1aa2e65e29932619d96f2711f69f0f9eac56', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:24:50', '2020-10-12 23:24:50', '2021-10-13 05:24:50'),
('aa4cc63e483a72bc88370a11eb99b75b4f387a3de77ed17d3827c9dbe81e4e53f195aabe3c0287f8', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 1, '2020-10-10 22:41:26', '2020-10-10 22:41:26', '2021-10-11 04:41:26'),
('ac3427332d63654f34fdb1165a9ea468486d1ebf9b4fc6fa7f9841ac6641d5bade5bd5633fa979b4', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:34:25', '2020-10-12 23:34:25', '2021-10-13 05:34:25'),
('b75c80878b541966541688b0d9fa129a89620ad5279171c14f68c1ec96e4a2e6f2b5ca78b867fc59', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 21:22:30', '2020-10-12 21:22:30', '2021-10-13 03:22:30'),
('cb783b43d5c2dbfe6b2453cfd49326376db0fe673c90a9225471adab7dc3eb482a9a19ca4e470c68', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:31:55', '2020-10-12 23:31:55', '2021-10-13 05:31:55'),
('f98917de1f020944f9d0f718545197e03b810d3996714b17051cae4468c8ef19831fd9587d67ebe8', 18, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 04:52:38', '2020-10-12 04:52:38', '2021-10-12 10:52:38'),
('fde8ffab38943987e754b315265647d1059aceec7cba0a853220456a926c4d537ba66d8bd92bd97e', 12, 1, 'Kijani ke hobe But APp ta colbe', '[]', 0, '2020-10-12 23:26:20', '2020-10-12 23:26:20', '2021-10-13 05:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Many Vendor Personal Access Client', 'SkO7SFLS2H6vXC7Yqec8yp55B7g2DJZTihOy7gC8', NULL, 'http://localhost', 1, 0, 0, '2020-10-07 01:20:13', '2020-10-07 01:20:13'),
(2, NULL, 'Many Vendor Password Grant Client', 'sd4an7vHOXKk0ZXZ6SpE3qO92bp6sFy0q6njx9kp', 'users', 'http://localhost', 0, 1, 0, '2020-10-07 01:20:13', '2020-10-07 01:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-07 01:20:13', '2020-10-07 01:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `area_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `order_number` bigint(20) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','delivered','canceled','follow_up','processing','quality_check','product_dispatched','confirmed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_star` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentedBy` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `page_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_contents`
--

CREATE TABLE `page_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `meta_data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_groups`
--

CREATE TABLE `page_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `current_balance` double DEFAULT NULL,
  `process` enum('Bank','Paypal','Stripe') COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Request','Confirm') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_change_date` datetime DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(82, 'dashboard', 'dashboard', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">dashboard</pre>', NULL, '2020-08-12 03:10:28', '2020-08-12 03:10:28'),
(83, 'user management', 'user-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">user-management</pre>', NULL, '2020-08-12 03:10:35', '2020-08-12 03:10:35'),
(84, 'user setup', 'user-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">user-setup</pre>', NULL, '2020-08-12 03:10:40', '2020-08-12 03:10:40'),
(85, 'group setup', 'group-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">group-setup</pre>', NULL, '2020-08-12 03:10:45', '2020-08-12 03:10:45'),
(86, 'manage permissions', 'permissions-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">permissions-manage</pre>', NULL, '2020-08-12 03:10:51', '2020-08-12 03:10:51'),
(87, 'mail configuration', 'mail-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">mail-setup</pre>', NULL, '2020-08-12 03:10:56', '2020-08-12 03:10:56'),
(88, 'site settings', 'site-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">site-setting</pre>', NULL, '2020-08-12 03:11:01', '2020-08-12 03:11:01'),
(89, 'language setup', 'language-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">language-setup</pre>', NULL, '2020-08-12 03:11:07', '2020-08-12 03:11:07'),
(90, 'currency setup', 'currency-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">currency-setup</pre>', NULL, '2020-08-12 03:11:12', '2020-08-12 03:11:12'),
(91, 'manage pages', 'pages-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">pages-manage</pre>', NULL, '2020-08-12 03:11:40', '2020-08-12 03:11:40'),
(92, 'category management', 'category-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">category-management</pre>', NULL, '2020-08-12 03:11:50', '2020-08-12 03:11:50'),
(93, 'commission management', 'commission-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">commission-management</pre>', NULL, '2020-08-12 03:11:55', '2020-08-12 03:11:55'),
(94, 'section settings', 'section-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">section-setting</pre>', NULL, '2020-08-12 03:12:26', '2020-08-12 03:12:26'),
(95, 'additional setting', 'additional-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">additional-setting</pre>', NULL, '2020-08-12 03:12:33', '2020-08-12 03:12:33'),
(96, 'manage product variant', 'product-variant-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">product-variant-manage</pre>', NULL, '2020-08-12 03:12:38', '2020-08-12 03:12:38'),
(97, 'manage product', 'product-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">product-manage</pre>', NULL, '2020-08-12 03:12:44', '2020-08-12 03:12:44'),
(98, 'ecommerce setting', 'ecommerce-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">ecommerce-setting</pre>', NULL, '2020-08-12 03:12:49', '2020-08-12 03:12:49'),
(99, 'manage brand', 'brand-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">brand-manage</pre>', NULL, '2020-08-12 03:12:53', '2020-08-12 03:12:53'),
(100, 'manage campaign', 'campaign-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">campaign-manage</pre>', NULL, '2020-08-12 03:12:58', '2020-08-12 03:12:58'),
(101, 'payment method setup', 'payment-method-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">payment-method-setup</pre>', NULL, '2020-08-12 03:13:26', '2020-08-12 03:13:26'),
(102, 'promotions banner setup', 'promotions-banner-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">promotions-banner-setup</pre>', NULL, '2020-08-12 03:13:32', '2020-08-12 03:13:32'),
(103, 'main slider', 'main-slider-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">main-slider-setup</pre>', NULL, '2020-08-12 03:13:37', '2020-08-12 03:13:37'),
(104, 'shipping setup', 'shipping-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">shipping-setup</pre>', NULL, '2020-08-12 03:13:44', '2020-08-12 03:13:44'),
(105, 'coupon setup', 'coupon-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">coupon-setup</pre>', NULL, '2020-08-12 03:13:50', '2020-08-12 03:13:50'),
(106, 'order manage', 'order-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">order-manage</pre>', NULL, '2020-08-12 03:13:56', '2020-08-12 03:13:56'),
(107, 'fullfillment', 'fullfill-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">fullfill-manage</pre>', NULL, '2020-08-12 03:14:01', '2020-08-12 03:14:01'),
(108, 'manage complain', 'complain-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">complain-manage</pre>', NULL, '2020-08-12 03:14:07', '2020-08-12 03:14:07'),
(109, 'admin earning', 'admin-earning', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">admin-earning</pre>', NULL, '2020-08-12 03:14:13', '2020-08-12 03:14:13'),
(110, 'seller', 'seller', NULL, NULL, '2020-08-12 03:35:48', '2020-08-12 03:35:48'),
(111, 'customer', 'customer', NULL, NULL, '2020-08-12 03:35:57', '2020-08-12 03:35:57'),
(112, 'seller management', 'seller-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">seller.management</span></pre>', NULL, '2020-08-12 05:05:42', '2020-08-12 05:05:42'),
(113, 'order-modify', 'order-modify', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">order-modify</span></pre>', NULL, '2020-08-17 20:57:54', '2020-08-17 20:57:54'),
(114, 'seller-payment', 'seller-payment', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">seller-payment</span></pre>', NULL, '2020-08-18 03:18:53', '2020-08-18 03:18:53'),
(115, 'switch mode', 'app-active', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">app-active</span></pre>', NULL, '2020-08-25 04:35:05', '2020-08-25 04:35:05'),
(116, 'addons-manager', 'addons-manager', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` bigint(20) DEFAULT NULL,
  `short_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `big_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` enum('youtube','vimeo') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_request` tinyint(1) NOT NULL DEFAULT 0,
  `have_variant` tinyint(1) NOT NULL DEFAULT 0,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `tax` double NOT NULL DEFAULT 0,
  `product_price` double DEFAULT NULL,
  `purchase_price` double DEFAULT NULL,
  `is_discount` tinyint(1) DEFAULT NULL,
  `discount_type` enum('flat','per') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` double DEFAULT NULL,
  `discount_percentage` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `unit` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_stocks`
--

CREATE TABLE `product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT 0,
  `alert_quantity` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `type` enum('header','category','section','mainSlider','popup') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section_settings`
--

CREATE TABLE `section_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `active` double NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blade_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section_settings`
--

INSERT INTO `section_settings` (`id`, `active`, `sort`, `name`, `blade_name`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Hero Section', 'main-banner', 'images/section/main-banner.png', NULL, NULL, '2020-10-23 23:46:30'),
(2, 1, 2, 'Trending Category', 'search-trending', 'images/section/search-trending.png', NULL, NULL, '2020-10-23 23:46:36'),
(3, 1, 3, 'Campaigns', 'deal-day', 'images/section/deal-day.png', NULL, NULL, '2020-10-23 23:46:36'),
(4, 1, 4, 'Brands', 'shop-brand', 'images/section/shop-brand.png', NULL, NULL, NULL),
(5, 1, 5, 'Shops', 'shop-store', 'images/section/shop-store.png', NULL, NULL, NULL),
(6, 1, 6, 'Promotions', 'promotional-banner', 'images/section/promotional-banner.png', NULL, NULL, NULL),
(7, 1, 7, 'Categories', 'top-categories', 'images/section/top-categories.png', NULL, NULL, NULL),
(8, 1, 8, 'Popular categories', 'category-promotional', 'images/section/category-promotional.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seller_accounts`
--

CREATE TABLE `seller_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Bank',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `routing_number` int(11) DEFAULT NULL,
  `paypal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Paypal',
  `paypal_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Stripe',
  `stripe_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_holder_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seller_earnings`
--

CREATE TABLE `seller_earnings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_stock_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `commission_pay` double DEFAULT NULL,
  `get_amount` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'default_currencies', '1', NULL, NULL),
(2, 'type_logo', 'uploads/site/TxlH0sLrU22wiJ1ev0HELuOsts0CsIh592cRCuKN.png', NULL, '2020-09-22 06:05:56'),
(3, 'type_name', 'Many Vendor', NULL, '2020-09-22 06:05:56'),
(4, 'type_footer', 'Manyvendor', NULL, '2020-09-22 06:05:56'),
(5, 'type_mail', 'manyvendor@gmail.com', NULL, '2020-09-22 06:05:56'),
(6, 'type_address', 'Dhaka Uttara', NULL, '2020-09-26 21:34:26'),
(7, 'type_fb', 'facebook.com/manyvendor', NULL, '2020-09-22 06:05:56'),
(8, 'type_tw', 'twitter.com/manyvendor', NULL, '2020-09-22 06:05:56'),
(9, 'type_number', '01825731327', NULL, '2020-09-22 06:05:56'),
(10, 'type_google', 'google.com/manyvendor', NULL, '2020-09-22 06:05:56'),
(11, 'footer_logo', NULL, NULL, '2020-08-06 00:07:09'),
(12, 'favicon_icon', 'uploads/site/WU01pUNSdsLDKpHitn3iNRutY1JbXgaY7Po6H1OS.png', NULL, '2020-09-22 06:05:56'),
(13, 'seller', 'enable', NULL, NULL),
(14, 'primary_color', NULL, NULL, '2020-08-06 00:44:26'),
(15, 'secondary_color', NULL, NULL, '2020-08-06 00:44:26'),
(16, 'seller_mode', 'on', NULL, '2020-08-06 00:44:26'),
(17, 'verification', NULL, NULL, '2020-08-06 00:44:27'),
(18, 'login_modal', 'on', NULL, '2020-08-06 00:44:27'),
(19, 'payment_logo', 'uploads/logo/8iN3FR7aoQlaKtTUpZG4txLVG3vF7q5OXtNqWQ6p.png', NULL, '2020-08-09 23:31:44'),
(20, 'type_ios', 'ertert', NULL, '2020-10-22 09:47:51'),
(21, 'type_appstore', 'uploads/site/ItQJHlwNBesLo098tuYq5lFFKtz9XKoTT0SvYzW8.png', NULL, '2020-10-22 09:55:18'),
(22, 'type_playstore', 'uploads/site/HrxX6heoPShhSqt2DFyxYeu3We2BU2Fz7Fve40cY.png', NULL, '2020-10-22 09:55:18'),
(23, 'type_android', 'rtert', NULL, '2020-10-22 09:47:51');

-- --------------------------------------------------------

--
-- Table structure for table `thanas`
--

CREATE TABLE `thanas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thana_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genders` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT 0,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar.png',
  `login_time` timestamp NULL DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('Admin','Customer','Vendor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_groups`
--

CREATE TABLE `user_has_groups` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_has_groups`
--

INSERT INTO `user_has_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 2),
(9, 2),
(19, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_permissions`
--

CREATE TABLE `user_has_permissions` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` float DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_licence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_logo` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approve_status` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_products`
--

CREATE TABLE `vendor_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_price` double NOT NULL,
  `purchase_price` double DEFAULT NULL,
  `is_discount` tinyint(1) DEFAULT NULL,
  `discount_type` enum('flat','per') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` double DEFAULT NULL,
  `discount_percentage` double DEFAULT NULL,
  `have_variant` tinyint(1) NOT NULL DEFAULT 0,
  `stock` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_variant_stocks`
--

CREATE TABLE `vendor_product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT 0,
  `alert_quantity` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_commissions`
--
ALTER TABLE `admin_commissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_products`
--
ALTER TABLE `campaign_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissions`
--
ALTER TABLE `commissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_campaign_products`
--
ALTER TABLE `ecom_campaign_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_carts`
--
ALTER TABLE `ecom_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_orders`
--
ALTER TABLE `ecom_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_order_products`
--
ALTER TABLE `ecom_order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_product_variant_stocks`
--
ALTER TABLE `ecom_product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infopages`
--
ALTER TABLE `infopages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logistics`
--
ALTER TABLE `logistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logistic_areas`
--
ALTER TABLE `logistic_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_has_permissions`
--
ALTER TABLE `module_has_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_contents`
--
ALTER TABLE `page_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_groups`
--
ALTER TABLE `page_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variant_stocks`
--
ALTER TABLE `product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_settings`
--
ALTER TABLE `section_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_earnings`
--
ALTER TABLE `seller_earnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thanas`
--
ALTER TABLE `thanas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_products`
--
ALTER TABLE `vendor_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_product_variant_stocks`
--
ALTER TABLE `vendor_product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_commissions`
--
ALTER TABLE `admin_commissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaign_products`
--
ALTER TABLE `campaign_products`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commissions`
--
ALTER TABLE `commissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_campaign_products`
--
ALTER TABLE `ecom_campaign_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_carts`
--
ALTER TABLE `ecom_carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_orders`
--
ALTER TABLE `ecom_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_order_products`
--
ALTER TABLE `ecom_order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_product_variant_stocks`
--
ALTER TABLE `ecom_product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `infopages`
--
ALTER TABLE `infopages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logistics`
--
ALTER TABLE `logistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logistic_areas`
--
ALTER TABLE `logistic_areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `module_has_permissions`
--
ALTER TABLE `module_has_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_contents`
--
ALTER TABLE `page_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_groups`
--
ALTER TABLE `page_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variant_stocks`
--
ALTER TABLE `product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `section_settings`
--
ALTER TABLE `section_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seller_earnings`
--
ALTER TABLE `seller_earnings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `thanas`
--
ALTER TABLE `thanas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_products`
--
ALTER TABLE `vendor_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_product_variant_stocks`
--
ALTER TABLE `vendor_product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
