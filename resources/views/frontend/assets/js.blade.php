<script async src="{{ asset('frontend/plugins/jquery.js') }}"></script>
<script async src="{{ asset('frontend/plugins/popper.js') }}"></script>
<script async src="{{ asset('frontend/plugins/owl-carousel/owl.carousel.js') }}"></script>
<script async src="{{ asset('frontend/plugins/bootstrap4/js/bootstrap.js') }}"></script>
<script async src="{{ asset('frontend/plugins/imagesloaded.pkgd.js') }}"></script>
<script async src="{{ asset('frontend/plugins/masonry.pkgd.js') }}"></script>
<script async src="{{ asset('frontend/plugins/isotope.pkgd.js') }}"></script>
<script async src="{{ asset('frontend/plugins/jquery.matchHeight-min.js') }}"></script>
<script async src="{{ asset('frontend/plugins/slick/slick/slick.js') }}"></script>
<script async src="{{ asset('frontend/plugins/jquery-bar-rating/dist/jquery.barrating.js') }}"></script>
<script async src="{{ asset('frontend/plugins/slick-animation.js') }}"></script>
<script async src="{{ asset('frontend/plugins/lightGallery-master/dist/js/lightgallery-all.js') }}"></script>
<script async src="{{ asset('frontend/plugins/jquery-ui/jquery-ui.js') }}"></script>
<script async src="{{ asset('frontend/plugins/sticky-sidebar/dist/sticky-sidebar.js') }}"></script>
<script async src="{{ asset('frontend/plugins/jquery.slimscroll.js') }}"></script>
<script async src="{{ asset('frontend/plugins/select2/dist/js/select2.full.js') }}"></script>
<script async src="{{ asset('js/parsley.js') }}"></script>
<script async src="{{ asset('js/jquery.cookie.js') }}"></script>
<!-- custom scripts-->
<script async src="{{ asset('frontend/js/main.js') }}"></script>
<script async src="{{ asset('js/toastr.js') }}"></script>


<script src="{{ asset('frontend/js/sub-menu.js') }}"></script>

