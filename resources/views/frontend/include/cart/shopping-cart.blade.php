<div class="ps-panel--sidebar" id="cart-mobile">
    <div class="ps-panel__header">
        <h3>@translate(Shopping Cart)</h3>
    </div>
    <div class="navigation__content">
        <div class="ps-cart--mobile">
            <div class="ps-cart__content">
                <div class="ps-product--cart-mobile">


                    

                    <input type="hidden" id="check-out-url" value="{{ route('checkout.index') }}">

                    <input type="hidden" id="shopping-cart-url" value="{{ route('shopping.cart') }}">


                    {{-- guest.checkout.index --}}
                    {{-- @if(guestCheckout()) --}}
                        <input type="hidden" id="guest-check-out-url" value="{{ route('guest.checkout.index') }}">


                        <input type="hidden" id="guest-shopping-cart-url" value="{{ route('guest.shopping.cart') }}">

                        

                    {{-- @endif --}}

                    <span class="show-cart-items">
                          {{-- data coming from ajax --}}
                    </span>
                </div>
            </div>
            <div class="ps-cart__footer cart-items-footer">
                {{-- data coming from ajax --}}
            </div>
        </div>
    </div>
</div>
