@extends('frontend.master')

@section('css')
    {{-- css goes here --}}
@stop

@section('title') @translate(Register) @stop

@section('content')
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{route('homepage')}}">@translate(Home)</a></li>
                <li>@translate(My account)</li>
            </ul>
        </div>
    </div>




    <div class="ps-my-account">
        <div class="container">
            <div class="ps-form--account ps-tab-root">
                <div class="ps-tab-list">
                    <span class="p-2 fs-28  font-weight-bold"><a href="{{route('login')}}"
                                                                 class="{{request()->is('login') ? 'color-active' : null}}">@translate(Login)</a></span>
                    <span class="p-2 fs-28 font-weight-bold"><a href="{{route('register')}}"
                                                                class="{{request()->is('register') ? 'color-active' : null}}">@translate(Register)</a></span>
                </div>
                <div class="card card-primary card-outline">
                    {{-- Flash message after successful registration --}}
                    <div class="m-4">
                    @if (Session::has('status'))
                        <div class="alert alert-info text-center">{{ Session::get('status') }}</div>
                    @endif
                    @if (Session::has('warning'))
                        <div class="alert alert-info text-center">{{ Session::get('warning') }}</div>
                    @endif
                    </div>
                    <div class="card-body" id="sign-in">
                        <div class="ps-form__content">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <h5>@translate(Log In Your Account)</h5>
                                <div class="form-group">
                                    <input class="form-control @error('email') is-invalid @enderror" type="email"
                                           name="email" placeholder="Email address" required>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                                <div class="form-group form-forgot">
                                    <input class="form-control @error('password') is-invalid @enderror" type="password"
                                           name="password" placeholder="Password" required>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                    <a href="{{ route('password.request') }}">@translate(Forgot)?</a>
                                </div>
                                <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control" type="checkbox" id="remember-me" name="remember">
                                        <label for="remember-me">@translate(Rememeber me)</label>
                                    </div>
                                </div>
                                <div class="form-group submtit">
                                    <button class="ps-btn ps-btn--fullwidth" type="submit">@translate(Login)</button>
                                </div>
                            </form>
                        </div>


                        <div class="ps-form__footer">
                            <p>@translate(Connect with):</p>
                            <ul class="ps-list--social">
                                
                                    <li><a class="facebook" href="{{ url('/auth/redirect/facebook') }}"><i
                                                    class="fa fa-facebook"></i></a></li>

                                    <li><a class="google" href="{{ url('/auth/redirect/google') }}"><i class="fa fa-google-plus"></i></a></li>
                                

                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@section('js')
    {{-- js goes here --}}
@stop
